package es.uned.edomdoo.forms;

import es.uned.edomdoo.entidades.Offer;

/**
 * Clase de apoyo para el formulario de ofertas
 * @author oscargt90
 *
 */
public class OfferForm {

	private Offer oferta;
	private Long selectArticleId;

	/**
	 * Constructor
	 * @param oferta oferta
	 */
	public OfferForm(Offer oferta) {
		this.oferta = oferta;
		setArticulo();
	}
	
	/**
	 * Devuelve la oferta
	 * @return oferta
	 */
	public Offer getOferta() {
		return oferta;
	}

	/**
	 * Establece la oferta
	 * @param oferta oferta
	 */
	public void setOferta(Offer oferta) {
		this.oferta = oferta;
		setArticulo();
	}

	/**
	 * Devuelve el artículo seleccionado
	 * @return id
	 */
	public Long getSelectArticleId() {
		return selectArticleId;
	}

	/**
	 * Establece el artículo seleccionado
	 * @param selectArticleId id
	 */
	public void setSelectArticleId(Long selectArticleId) {
		this.selectArticleId = selectArticleId;
	}

	/**
	 * Indica si se ha cambiado de artículo
	 * @return true o false
	 */
	public boolean isArticleChanged() {
		if (oferta.getArticulo()==null) {
			return (selectArticleId!=-1);
		} else {
			return (selectArticleId!=oferta.getArticulo().getId());
		}
	}
	
	private void setArticulo() {
		if (oferta==null) return;
		if (oferta.getArticulo()!=null) {
			selectArticleId=oferta.getArticulo().getId();
		} else {
			selectArticleId= new Long(-1);
		}
	}
	
	
	
}
