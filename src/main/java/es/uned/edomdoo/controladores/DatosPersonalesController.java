package es.uned.edomdoo.controladores;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.User;
import es.uned.edomdoo.servicios.UserService;

/**
 * Controlador para modificar los datos personales
 * @author oscar
 *
 */
@Controller
public class DatosPersonalesController {

	final private UserService userService;


	/**
	 * Construcor
	 * @param uS servicio usuarios
	 */
	@Autowired
	public DatosPersonalesController(UserService uS) {
		this.userService = uS;
	}

	/**
	 * Mapeo de la página datos personales
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping(value = { "/datospersonales" })
	public String datosPersonales(Model model) {
		model.addAttribute("usuario", Sesion.getInstance().getUserLoged());
		model.addAttribute("clave", "");
		return "datospersonales";
	}
	
	/**
	 * Mapeo del formulario para grabar los datos personales
	 *  
	 * @param model modelo
	 * @param usuario usuario
	 * @return página
	 **/
	@RequestMapping(value = "/datospersonales/save", method = RequestMethod.POST)
	public String datosPersonalesSave(Model model, @Valid User usuario) {
		User usr = userService.findOne(usuario.getId());
		usr.setName(usuario.getName());
		usr.setLastname(usuario.getLastname());
		usr.setEmail(usuario.getLastname());
		userService.saveUser(usuario);
		return "/index";
	}	
	
	/**
	 * Mapeo del formulario para cambiar de clave
	 * 
	 * @param model modelo
	 * @param clave clave encriptada
	 * @return página
	 **/
	@RequestMapping(value = "/datospersonales/pwd", method = RequestMethod.POST)
	public String datosPersonalesPwd(Model model, @Valid String clave) {
		User usr = Sesion.getInstance().getUserLoged();
		usr.setPassword(clave);
		userService.saveUser(usr);
		return "/index";
	}
	
	/**
	 * Mapeo del formulario para darse de baja
	 *  
	 * @param model modelo
	 * @return página
	 **/
	@RequestMapping(value = "/datospersonales/baja", method = RequestMethod.POST)
	public String datosPersonalesBaja(Model model) {
		// Damos de baja al usuario
		User usr = Sesion.getInstance().getUserLoged();
		usr.setActive(false);
		userService.saveUser(usr);
		// Le damos de baja de la sesión
		Sesion.getInstance().logout();
		SecurityContextHolder.clearContext();
		return "/baja";
	}
	
}
