package es.uned.edomdoo.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.entidades.Vendor;
import es.uned.edomdoo.servicios.VendorService;

/**
 * Controlador para la gestión de tiendas
 * @author pablo
 *
 */
@Controller
public class VendorControl {
	
	final private VendorService vendorService;

	/**
	 * Constructor
	 * @param vS servicio tiendas
	 */
	@Autowired
	public VendorControl(VendorService vS) {
		this.vendorService = vS;		
	}
	
	/**
	 * Mapeo de la página listado de tiendas
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@RequestMapping(value = "admin/tiendas")
	public String VendorsList(Model model) {
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("tienda", new Vendor());
		return "admin/tiendas";
	}

	/**
	 * Mapeo para grabar editar una tienda
	 * 
	 * @param model modelo
	 * @param id tienda
	 * @return página
	 **/
	@RequestMapping(value = { "admin/tiendasEdit", "admin/tiendasEdit/{id}" }, method = RequestMethod.GET)
	public String VendorsEditForm(Model model, @PathVariable(required = false, name = "id") Long id) {
		if (null != id) {
			model.addAttribute("tienda", vendorService.findOne(id));
		} else {
			model.addAttribute("tienda", new Vendor());
		}
		return "admin/tiendasEdit";
	}

	/**
	 * Mapeo para grabar una tienda
	 * 
	 * @param model modelo
	 * @param vendor tienda
	 * @return página
	 **/
	@RequestMapping(value = "admin/tiendasEdit", method = RequestMethod.POST)
	public String VendorsEdit(Model model, @Valid Vendor vendor) {
		vendorService.saveVendor(vendor);
		model.addAttribute("tiendasList", vendorService.findAll());
		return "redirect:/admin/tiendas";
	}

	/**
	 * Mapeo para borrar una tienda
	 * 
	 * @param model modelo
	 * @param id tienda
	 * @return página
	 **/
	@RequestMapping(value = "admin/tiendasDelete/{id}", method = RequestMethod.GET)
	public String VendorsDelete(Model model, @PathVariable(required = true, name = "id") Long id) {
		vendorService.deleteVendor(id);
		model.addAttribute("tiendasList", vendorService.findAll());
		return "admin/tiendas";
	}

}
