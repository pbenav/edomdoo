package es.uned.edomdoo.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.entidades.Brand;
import es.uned.edomdoo.servicios.BrandService;

/**
 * Controlador para las marcas
 * 
 * @author pablo
 *
 */
@Controller
public class BrandControl {
	
	final private BrandService brandService;

	@Autowired
	public BrandControl(BrandService bS) {
		this.brandService = bS;		
	}
	
	/**
	 * Mapeo para el listado de marcas
	 * @param model modelo
	 * @return página
	 */
	@RequestMapping(value = "admin/marcas")
	public String BrandsList(Model model) {
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("marca", new Brand());
		return "admin/marcas";
	}

	/**
	 * Mapeo de la página ficha de marca
	 * 
	 * @param model modelo
	 * @param id marca
	 * @return página
	 **/
	@RequestMapping(value = { "admin/marcasEdit", "admin/marcasEdit/{id}" }, method = RequestMethod.GET)
	public String BrandsEditForm(Model model, @PathVariable(required = false, name = "id") Long id) {
		if (null != id) {
			model.addAttribute("marca", brandService.findOne(id));
		} else {
			model.addAttribute("marca", new Brand());
		}
		return "admin/marcasEdit";
	}

	/**
	 * Mapeo para grabar una marca
	 * 
	 * @param model modelo
	 * @param brand marca
	 * @return página
	 **/
	@RequestMapping(value = "admin/marcasEdit", method = RequestMethod.POST)
	public String BrandsEdit(Model model, @Valid Brand brand) {
		brandService.saveMarca(brand);
		model.addAttribute("marcasList", brandService.findAll());
		return "redirect:/admin/marcas";
	}

	/**
	 * Mapeo para borrar una marca
	 * 
	 * @param model modelo
	 * @param id marca
	 * @return página
	 **/
	@RequestMapping(value = "admin/marcasDelete/{id}", method = RequestMethod.GET)
	public String BrandsDelete(Model model, @PathVariable(required = true, name = "id") Long id) {
		brandService.deleteMarca(id);
		model.addAttribute("marcasList", brandService.findAll());
		return "admin/marcas";
	}
	
}
