package es.uned.edomdoo.controladores;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.config.Sesion;


/**
 * Controlador que decide dónde redirigir al usuario logueado, en función del role
 * @author oscargt90
 *
 */
@Controller
public class LoginControl {

	/**
	 * Mapeo de la página redirección al hacer login
	 * 
	 * @param request petición
	 * @return página
	 **/
    @RequestMapping("/default")
    public String defaultAfterLogin(HttpServletRequest request) {
    	// Si es administrador o tienda, redirige a la parte de administración
        if (Sesion.getInstance().isAdmin() || Sesion.getInstance().isVendor()) {
            return "redirect:/admin/";
        }
        // Es cliente, redirige a la pantalla principal
        return "redirect:/";
    }
    

	/**
	 * Mapeo de la página logout
	 * 
	 * @param request petición
	 * @param response respuesta
	 * @return página
	 **/
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    Sesion.getInstance().logout();
	    return "redirect:/login?logout";
	}

}
