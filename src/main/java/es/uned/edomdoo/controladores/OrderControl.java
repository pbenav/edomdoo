package es.uned.edomdoo.controladores;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.entidades.Order;
import es.uned.edomdoo.entidades.Vendor;
import es.uned.edomdoo.servicios.ArticleService;
import es.uned.edomdoo.servicios.BrandService;
import es.uned.edomdoo.servicios.CatService;
import es.uned.edomdoo.servicios.OrderService;
import es.uned.edomdoo.servicios.VendorService;

/**
 * Controlador para hacer consultas de pedidos
 * 
 * @author Óscar García-Tapia Armada
 *
 */
@Controller
public class OrderControl {

	private final VendorService vendorService;
	private final OrderService orderService;
	private final ArticleService articleService;
	final private BrandService brandService;
	final private CatService catService;
	
	/**
	 * Constructor para la gestión de pedidos
	 * @param vS servicio tiendas
	 * @param oS servicio pedidos
	 * @param aS servicio artículos
	 * @param bS servicio marcas
	 * @param cS servicio categorías
	 */
	@Autowired
	public OrderControl(VendorService vS, OrderService oS, ArticleService aS,  BrandService bS, CatService cS) {
		this.vendorService = vS;
		this.orderService = oS;
		this.articleService = aS;
		this.brandService = bS;
		this.catService = cS;
	}

	/**
	 * Mapeo de la página listado de pedidos
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@RequestMapping(value = "admin/pedidos")
	public String pedidosList(Model model) {
		// Si es una tienda, cargamos los pedidos de esa tienda
		List<Order> listaPedidos;
		if (Sesion.getInstance().isVendor()) {
			Long vendorid = Sesion.getInstance().getUserLoged().getVendorId();
			Vendor vendor = vendorService.findOne(vendorid);
			listaPedidos = orderService.findByVendor(vendor);
		} else {
			if (Sesion.getInstance().isCustomer()) {
				listaPedidos = orderService.findByUser(Sesion.getInstance().getUserLoged());
			} else {
				listaPedidos = orderService.findAll();
			}
		}
		model.addAttribute("vendor", vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId()));
		listaPedidos.clear();
		model.addAttribute("pedidosList", listaPedidos);
		model.addAttribute("total", total(listaPedidos));		
		return "admin/pedidos";
	}	
	/**
	 * Mapeo de la página listado de pedidos por fecha
	 * 
	 * @param model modelo
	 * @param fecha fecha del pedido
	 * @return página
	 **/
	@RequestMapping(value = "/admin/pedidosfecha/{fecha}", method = RequestMethod.POST)
	public String ventasPorFecha(Model model, @Valid String fecha) {
		// Cargamos los pedidos de esa tienda
		Vendor vendor = vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId());
		List<Order> listaPedidos;
		// Si no se introduce fecha, se cargan todos los pedidos
		if (fecha.isEmpty()) {
			listaPedidos = orderService.findByVendor(vendor);

		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dia = new Date();
		    try {
				dia = sdf.parse(fecha);
				listaPedidos = orderService.findByDateAndVendor(dia, vendor);
			} catch (ParseException e) {
				// Si hay un error cargamos la página vacía
				return pedidosList(model);
			}			
		}
		model.addAttribute("vendor",vendor);
		model.addAttribute("pedidosList", listaPedidos);
		model.addAttribute("total", total(listaPedidos));
		return "admin/pedidos";
	}	

	/**
	 * Mapeo para borrar un pedido
	 * 
	 * @param model modelo
	 * @param id identificador del pedido
	 * @return página
	 **/
	@RequestMapping(value = "/admin/pedidosdel/{id}")
	public String deleteOrder(Model model, @PathVariable(required = false, name = "id") Long id) {
		// Si es una tienda, cargamos los pedidos de esa tienda
		Optional<Order> oo = orderService.findById(id);
		Long uds = oo.get().getUnidades();
		Article art = oo.get().getArticulo();
		// Borramos el pedido
		orderService.deleteOrder(id);
		// Recalculamos el nuevo stock
		int stock = art.getStock() + uds.intValue();
		art.setStock(stock);
		// Actualizamos el stock
		articleService.saveArticulo(art);
		return pedidosList(model);
		//return "admin/pedidos";
	}
	
		
	/**
	 * Mapeo de la página historial de pedidos de un usuario
	 * @param model Model 
	 * @return página
	 **/
	@RequestMapping(value = { "/historial"})
	public String historial(Model model) {
		List<Order> listaPedidos;
		// Long userid = Sesion.getInstance().getUserLoged().getId();
		listaPedidos = orderService.findByUser(Sesion.getInstance().getUserLoged());
		model.addAttribute("pedidosList", listaPedidos);
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "/historial";
	}
	
	private String total(List<Order> lista) {
		double total = 0.0;
		for (Order order: lista) {
			total =+ order.getTotal();
		}
		return String.format("%.2f", total) + "€";
	}
	
}
