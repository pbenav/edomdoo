package es.uned.edomdoo.controladores;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.Order;
import es.uned.edomdoo.entidades.Product;
import es.uned.edomdoo.entidades.User;
import es.uned.edomdoo.entidades.Vendor;
import es.uned.edomdoo.servicios.ArticleService;
import es.uned.edomdoo.servicios.BrandService;
import es.uned.edomdoo.servicios.CatService;
import es.uned.edomdoo.servicios.OrderService;
import es.uned.edomdoo.servicios.ProductService;
import es.uned.edomdoo.servicios.UserService;
import es.uned.edomdoo.servicios.VendorService;
import es.uned.edomdoo.storage.StorageProperties;

/**
 * Controlador con funcionalidades varias
 * @author pablo
 *
 */
@Controller
public class MiControlador {

	final private UserService userService;
	final private VendorService vendorService;
	final private BrandService brandService;
	final private CatService catService;
	final private ProductService productService;
	private final OrderService orderService;

	/**
	 * Constructor
	 * @param aS servicio artículos
	 * @param uS servicio usuarios
	 * @param bS servicio marcas
	 * @param vS servicio tiendas
	 * @param cS servicio categorías
	 * @param pS servicio productos
	 * @param oS servicio pedidos
	 */
	@Autowired
	public MiControlador(ArticleService aS, UserService uS, BrandService bS, VendorService vS, CatService cS,
			ProductService pS, OrderService oS) {
		this.userService = uS;
		this.vendorService = vS;
		this.brandService = bS;
		this.catService = cS;
		this.productService = pS;
		this.orderService = oS;
	}

	/**
	 * Mapeo de la página principal
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping(value = { "/" })
	public String index(Model model) {
		model.addAttribute("productosList", productService.findAll());
		List<Product> destacados = productService.findByCalDesc().subList(0, 4);
		model.addAttribute("destacadosList", destacados);
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "index";
	}

	/**
	 * Mapeo a ficha producto
	 * @param model modelo
	 * @param id producto
	 * @return página
	 */
	@RequestMapping(value = { "producto", "producto/{id}" }, method = RequestMethod.GET)
	public String articuloViewForm(Model model, @PathVariable(required = false, name = "id") Long id) {
		model.addAttribute("brand", brandService.findAll());
		model.addAttribute("category", catService.findAll());
		model.addAttribute("producto", productService.findOne(id));
		model.addAttribute("puntuado", false); // Para controlar si se ha puntuado el artículo
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "producto";
	}

	/**
	 * Mapeo de la página de la ficha de la tienda
	 * 
	 * @param model modelo
	 * @param id tienda
	 * @return página
	 **/
	@RequestMapping(value = { "tiendas", "tiendas/{id}" }, method = RequestMethod.GET)
	public String tiendas(Model model, @PathVariable(required = false, name = "id") Long id) {
		List<Product> destacados = new ArrayList<Product>();

		if (id != null) {
			model.addAttribute("productosList", productService.findByVid(id));
			for (Product product : productService.findByCalDesc()) {
				if (product.getId().equals(id)) {
					try {
						destacados.add(product);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			model.addAttribute("productosList", productService.findAll());
			destacados = productService.findByCalDesc();
		}

		if (destacados.size() > 4) {
			destacados = destacados.subList(0, 4);
		}

		model.addAttribute("destacadosList", destacados);
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "index";
	}

	/**
	 * Mapeo de la página de filtrado por marca
	 * 
	 * @param model modelo
	 * @param id id de la marca
	 * @return página
	 **/
	@RequestMapping(value = { "marcas", "marcas/{id}" }, method = RequestMethod.GET)
	public String marcas(Model model, @PathVariable(required = false, name = "id") Long id) {
		List<Product> destacados = new ArrayList<Product>();

		if (id != null) {
			model.addAttribute("productosList", productService.findByBrand(id));
			for (Product product : productService.findByCalDesc()) {
				if (product.getId().equals(id)) {
					try {
						destacados.add(product);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			model.addAttribute("productosList", productService.findAll());
			destacados = productService.findByCalDesc();
		}

		if (destacados.size() > 4) {
			destacados = destacados.subList(0, 4);
		}

		model.addAttribute("destacadosList", destacados);
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "index";
	}

	/**
	 * Mapeo de la página de filtrado por categorías
	 * 
	 * @param model modelo
	 * @param id id de la categoría
	 * @return página
	 **/
	@RequestMapping(value = { "categorias", "categorias/{id}" }, method = RequestMethod.GET)
	public String categorias(Model model, @PathVariable(required = false, name = "id") Long id) {
		List<Product> destacados = new ArrayList<Product>();

		if (id != null) {
			model.addAttribute("productosList", productService.findByCategory(id));
			for (Product product : productService.findByCalDesc()) {
				if (product.getId().equals(id)) {
					try {
						destacados.add(product);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			model.addAttribute("productosList", productService.findAll());
			destacados = productService.findByCalDesc();
		}

		if (destacados.size() > 4) {
			destacados = destacados.subList(0, 4);
		}

		model.addAttribute("destacadosList", destacados);
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "index";
	}

	/**
	 * Mapeo de la página de login
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping("/login")
	public String login(Model model) {
		model.addAttribute("usuario", new User());
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		return "login";
	}

	/**
	 * Mapeo de la página de registro
	 * 
	 * @param model modelo
	 * @param user usuario
	 * @return página
	 **/
	@PostMapping(value = "/register")
	public String Register(Model model, @Valid User user) {
		System.out.println("-- Nuevo usuario: " + user.toString() + "--");
		System.out.println("-- Usuario, Rol: " + user.getName() + "-" + user.getRole() + "--");
		Optional<User> actUsr = userService.findByUsername(user.getUsername());
		if (actUsr.isPresent()) {
			System.out.println("-- El usuario ya existe. --");
			model.addAttribute("mensaje", "El usuario ya existe.");
			model.addAttribute("usuario", new User());
			return "login";
		} else {
			user.setActive(Boolean.TRUE);
			userService.saveUser(user);
		}
		model.addAttribute("usuario", userService.findOne(user.getId()));
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		return "index";
	}

	/**
	 * Mapeo del backend para administradores
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@RequestMapping(value = "/admin")
	public String admin(Model model) {

		List<Order> listaPedidos;
		Long vendorid = Sesion.getInstance().getUserLoged().getVendorId();
		Vendor vendor = vendorService.findOne(vendorid);
		listaPedidos = orderService.findByVendor(vendor);

		model.addAttribute("vendor", vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId()));
		model.addAttribute("pedidosList", listaPedidos);
		model.addAttribute("total", total(listaPedidos));
		model.addAttribute("npedidos", listaPedidos.size());
		return "/admin/index";
	}

	private String total(List<Order> lista) {
		double total = 0.0;
		for (Order order : lista) {
			total = +order.getTotal();
		}
		return String.format("%.2f", total) + "€";
	}

	/**
	 * Mapeo de la página contacto
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping("/contacto")
	public String contacto(Model model) {
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "contact";
	}

	/**
	 * Mapeo de la página términos de uso
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping("/terminos")
	public String terminos(Model model) {
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "terms";
	}
	
	/**
	 * Mapeo de la página coockies
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping("/cookies")
	public String cookies(Model model) {
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "cookies";
	}

	/**
	 * Mapeo de la página sobre nosotros
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping("/sobrenosotros")
	public String sobrenosotros(Model model) {
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "about";
	}

	/**
	 * Mapeo de la página política de privacidad
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping("/politicaprivacidad")
	public String politicaprivacidad(Model model) {
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "privacy";
	}

	/**
	 * Mapeo de la página principal
	 * @param model modelo
	 * @return página
	 **/
	@GetMapping(value = { "/nuestrastiendas" })
	public String nuestrastiendas(Model model) {
		List<Vendor> tiendas = vendorService.findAll();
		model.addAttribute("tiendaList", tiendas);
		model.addAttribute("marcasList", brandService.findAll());
		model.addAttribute("tiendasList", vendorService.findAll());
		model.addAttribute("categoriasList", catService.findAll());
		return "/nuestrastiendas";
	}
	
}
