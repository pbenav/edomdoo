package es.uned.edomdoo.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.entidades.Brand;
import es.uned.edomdoo.entidades.Category;
import es.uned.edomdoo.entidades.Vendor;
import es.uned.edomdoo.servicios.ArticleService;
import es.uned.edomdoo.servicios.BrandService;
import es.uned.edomdoo.servicios.CatService;
import es.uned.edomdoo.servicios.VendorService;
import es.uned.edomdoo.storage.StorageService;

/**
 * Controlador para la gestión de artículos
 * 
 * @author pablo
 *
 */
@Controller
public class ArticleControl {

	final private ArticleService articuloService;
	private StorageService storageService;
	private VendorService vendorService;
	private BrandService brandService;
	private CatService catService;

	/**
	 * Constructor
	 * 
	 * @param aS
	 *            servicio de artículos
	 * @param vS
	 *            servicio de tiendas
	 * @param bS
	 *            servicio de marcas
	 * @param cS
	 *            servicio de categorías
	 */
	@Autowired
	public ArticleControl(ArticleService aS, VendorService vS, BrandService bS, CatService cS) {
		this.articuloService = aS;
		this.vendorService = vS;
		this.brandService = bS;
		this.catService = cS;
	}

	/**
	 * Subida de archivos
	 * 
	 * @param sS
	 *            servicio de almacenamiento
	 */
	@Autowired
	public void FileUploadControl(StorageService sS) {
		this.storageService = sS;
	}

	/**
	 * Mapeo de la página de artículos
	 * 
	 * @param model
	 *            modelo
	 * @return página
	 **/
	@RequestMapping(value = "admin/articulos")
	public String articulosList(Model model) {
		model.addAttribute("articulosList",
				articuloService.findByVid(Sesion.getInstance().getUserLoged().getVendorId()));
		Vendor v = vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId());
		model.addAttribute("vendor", v);
		model.addAttribute("brand", brandService.findAll());
		model.addAttribute("category", catService.findAll());
		model.addAttribute("articulo", new Article());
		return "admin/articulos";
	}

	/**
	 * Mapeo de la ficha de artículos
	 * 
	 * @param model
	 *            modelo
	 * @param id
	 *            artículo
	 * @return página
	 **/
	@RequestMapping(value = { "admin/articulosEdit", "admin/articulosEdit/{id}" }, method = RequestMethod.GET)
	public String articulosEditForm(Model model, @PathVariable(required = false, name = "id") Long id) {
		Article articulo;
		Long categoriaid = new Long(-1);
		Long marcaid = new Long(-1);
		if (null != id) {
			articulo = articuloService.findOne(id);
			categoriaid = (articulo.getCategory() == null ? -1 : articulo.getCategory().getId());
			marcaid = (articulo.getBrand() == null ? -1 : articulo.getBrand().getId());
		} else {
			model.addAttribute("brand", new Brand());
			articulo = new Article();
			model.addAttribute("category", new Category());
		}
		// model.addAttribute("vendor", vendorService.findAll());
		model.addAttribute("vendor", vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId()));
		model.addAttribute("articulo", articulo);
		model.addAttribute("brand", brandService.findAll());
		model.addAttribute("categories", catService.findAll());
		model.addAttribute("categoriaid", categoriaid);
		model.addAttribute("marcaid", marcaid);
		return "admin/articulosEdit";
	}

	/**
	 * Mapeo para grabar un artículo
	 * 
	 * @param model
	 *            modelo
	 * @param delImg
	 *            imagen
	 * @param articulo
	 *            artículo
	 * @param categoriaid
	 *            categoría
	 * @param marcaid
	 *            marca
	 * @param file
	 *            imagen
	 * @return página
	 **/
	@RequestMapping(value = "admin/articulosEdit", method = RequestMethod.POST)
	public String articulosEdit(Model model, @ModelAttribute("delImg") String delImg, @Valid Article articulo,
			@Valid Long categoriaid, @Valid Long marcaid, @RequestParam("file") MultipartFile file) {

		String filename;				
		filename = file.getOriginalFilename();
		
		if (articulo.getId() != null) {
			Article orig = articuloService.findOne(articulo.getId());
			articulo.setImg(orig.getImg());
		}
				
		if (delImg == "true") {
			articulo.setImg(null);
		}

		if (!filename.isEmpty()) {
			System.out.println("\nSubiendo: " + filename + "... \n");
			storageService.store(file);
			articulo.setImg(filename);
		}

		articulo.setBrand(brandService.findOne(marcaid));
		articulo.setCategory(catService.findOne(categoriaid));

		// Si es un artículo nuevo, le asignamos la tienda
		if (articulo.getVid() == null) {
			articulo.setVid(Sesion.getInstance().getUserLoged().getVendorId());
		}

		if (articulo.getImg() == null) {
			articulo.setImg("noimage.jpg");
		}
		System.out.println("\nNombre almacenado: " + articulo.toString() + "... \n");
		articuloService.saveArticulo(articulo);
		model.addAttribute("articulosList",
				articuloService.findByVid(Sesion.getInstance().getUserLoged().getVendorId()));
		model.addAttribute("vendor", vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId()));
		model.addAttribute("brand", brandService.findAll());
		model.addAttribute("category", catService.findAll());
		return "admin/articulos";
	}

	/**
	 * Mapeo para borrar un artículo
	 * 
	 * @param model
	 *            modelo
	 * @param id
	 *            artículo
	 * @return página
	 **/
	@RequestMapping(value = "admin/articulosDelete/{id}", method = RequestMethod.GET)
	public String articulosDelete(Model model, @PathVariable(required = true, name = "id") Long id) {
		// articuloService.deleteArticulo(id);
		// Article art = articuloService.findOne(id);
		articuloService.deleteArticulo(id);
		model.addAttribute("articulosList",
				articuloService.findByVid(Sesion.getInstance().getUserLoged().getVendorId()));
		Vendor v = vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId());
		model.addAttribute("vendor", v);
		model.addAttribute("brand", brandService.findAll());
		model.addAttribute("category", catService.findAll());
		model.addAttribute("articulo", new Article());
		return articulosList(model);
	}

}
