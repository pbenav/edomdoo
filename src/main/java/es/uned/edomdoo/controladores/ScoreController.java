package es.uned.edomdoo.controladores;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.servicios.ProductService;
import es.uned.edomdoo.servicios.ScoreService;

/**
 * Controlador para gestionar las puntuaciones
 * @author oscar
 *
 */
@Controller
public class ScoreController {

	final private ScoreService scoreService;
	final private ProductService productService;

	/**
	 * Constructor
	 * @param pS servico productos
	 * @param sS servicio puntuaciones
	 */
	@Autowired
	public ScoreController(ProductService pS, ScoreService sS) {
		productService = pS;
		scoreService = sS;
	}


	/**
	 * Mapeo para puntuar un producto
	 * @param model modelo
	 * @param id producto
	 * @param pts puntos
	 * @return página
	 */
	@RequestMapping(value = { "puntuar", "puntuar/{id}/{pts}" }, method = RequestMethod.GET)
	public String puntuar(Model model, @PathVariable(required = false, name = "id") Long id, @PathVariable("pts") Integer pts) {
		// Guardamos la puntuación
		scoreService.addScore(Sesion.getInstance().getUserLoged().getId(), id, pts);
		model.addAttribute("producto", productService.findOne(id));
		model.addAttribute("puntuado", true);
		return "producto";
	}
	

}
