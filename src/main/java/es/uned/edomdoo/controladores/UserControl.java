package es.uned.edomdoo.controladores;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.User;
import es.uned.edomdoo.entidades.Vendor;
import es.uned.edomdoo.servicios.UserService;
import es.uned.edomdoo.servicios.VendorService;

/**
 * Controlador para la gestión de usuarios
 * 
 * @author pablo
 *
 */
@Controller
public class UserControl {
	
	final private UserService userService;
	final private VendorService vendorService;

	/**
	 * Constructor
	 * @param uS servicio usuarios
	 * @param vS servicio tiendas
	 */
	@Autowired
	public UserControl(UserService uS, VendorService vS) {
		this.userService = uS;		
		vendorService = vS;
	}
	
	/**
	 * Mapeo de la página listado de usuarios
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@RequestMapping(value = "admin/usuarios")
	public String UsersList(Model model) {
		model.addAttribute("usuariosList", userService.findAll());
		model.addAttribute("usuario", new User());
		return "admin/usuarios";
	}

	/**
	 * Mapeo de la página ficha de usuario
	 * 
	 * @param model modelo
	 * @param id usuario
	 * @return página
	 **/
	@RequestMapping(value = { "admin/usuariosEdit", "admin/usuariosEdit/{id}" }, method = RequestMethod.GET)
	public String UsersEditForm(Model model, @PathVariable(required = false, name = "id") Long id) {
		if (null != id) {
			model.addAttribute("usuario", userService.findOne(id));
		} else {
			model.addAttribute("usuario", new User());
		}
		// Cargamos la lista de tiendas
		List<Vendor> lista = vendorService.findAll();
		model.addAttribute("tiendasList", lista);
		return "admin/usuariosEdit";
	}

	/**
	 * Mapeo para grabar un usuario
	 * 
	 * @param model modelo
	 * @param user usuario
	 * @return página
	 **/
	@RequestMapping(value = "admin/usuariosEdit", method = RequestMethod.POST)
	public String UsersEdit(Model model, @Valid User user) {
		// Comprobamos si hemos seleccionado o no la tienda
		if (user.getVendorId()==-1) {
			// No hemos seleccionado tienda, si el role es tienda, lo establecemos en customer
			if (user.getRole().equals(Sesion.ROLE_VENDOR)) user.setRole(Sesion.ROLE_CUSTOMER);
		} else {
			// Si hemos seleccionado tienda, forzamos role vendor
			user.setRole(Sesion.ROLE_VENDOR);
		}		
		userService.saveUser(user);
		model.addAttribute("usuariosList", userService.findAll());
		return "redirect:/admin/usuarios";
	}

	/**
	 * Mapeo para borrar un usuario
	 * 
	 * @param model modelo
	 * @param id usuario
	 * @return página
	 **/
	@RequestMapping(value = "admin/usuariosDelete/{id}", method = RequestMethod.GET)
	public String UsersDelete(Model model, @PathVariable(required = true, name = "id") Long id) {
		userService.deleteUser(id);
		model.addAttribute("usuariosList", userService.findAll());
		return "admin/usuarios";
	}
	
}
