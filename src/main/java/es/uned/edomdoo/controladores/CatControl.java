package es.uned.edomdoo.controladores;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uned.edomdoo.entidades.Category;
import es.uned.edomdoo.servicios.CatService;

/**
 * Controlador para la gestión de categorías
 * 
 * @author pablo
 *
 */
@Controller
public class CatControl {
	
	final private CatService catService;

	@Autowired
	public CatControl(CatService cS) {
		this.catService = cS;		
	}
	
	/**
	 * Mapeo de la página listado de categorías
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@RequestMapping(value = "admin/categorias")
	public String CategoriesList(Model model) {
		model.addAttribute("categoriasList", catService.findAll());
		model.addAttribute("categoria", new Category());
		return "admin/categorias";
	}

	/**
	 * Mapeo de la página edición de categorías
	 * 
	 * @param model modelo
	 * @param id categoría
	 * @return página
	 **/
	@RequestMapping(value = { "admin/categoriasEdit", "admin/categoriasEdit/{id}" }, method = RequestMethod.GET)
	public String CategoriesEditForm(Model model, @PathVariable(required = false, name = "id") Long id) {
		if (null != id) {
			model.addAttribute("categoria", catService.findOne(id));
		} else {
			model.addAttribute("categoria", new Category());
		}
		return "admin/categoriasEdit";
	}

	/**
	 * Mapeo para grabar una categoría modificada
	 * 
	 * @param model modelo
	 * @param cat categoría
	 * @return página
	 **/
	@RequestMapping(value = "admin/categoriasEdit", method = RequestMethod.POST)
	public String CategoriesEdit(Model model, @Valid Category cat) {
		catService.saveCategoria(cat);
		model.addAttribute("categoriasList", catService.findAll());
		return "redirect:/admin/categorias";
	}

	/**
	 * Mapeo para borrar una categoría
	 * 
	 * @param model modelo
	 * @param id categoría
	 * @return página
	 **/
	@RequestMapping(value = "admin/categoriasDelete/{id}", method = RequestMethod.GET)
	public String CategoriesDelete(Model model, @PathVariable(required = true, name = "id") Long id) {
		catService.deleteCategoria(id);
		model.addAttribute("categoriasList", catService.findAll());
		return "admin/categorias";
	}
	
}
