package es.uned.edomdoo.controladores;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.Offer;
import es.uned.edomdoo.forms.OfferForm;
import es.uned.edomdoo.servicios.ArticleService;
import es.uned.edomdoo.servicios.OfferService;
import es.uned.edomdoo.servicios.VendorService;

/**
 * Controlador para la gestión de ofertas
 * @author Óscar García-Tapia Armada
 *
 */
@Controller
public class OfferControl {

	private final OfferService ofertaService;
	private final VendorService vendorService;
	private final ArticleService articleService;
	
	/**
	 * Constructor
	 * @param oS servicio de ofertas
	 * @param vS servicio tiendas
	 * @param aS servicio artículos
	 */
	@Autowired
	public OfferControl(OfferService oS, VendorService vS, ArticleService aS) {
		ofertaService = oS;
		vendorService = vS;
		articleService = aS;
	}

	/**
	 * Mapeo de la página listado de ofertas
	 * 
	 * @param model modelo
	 * @return página
	 **/
	@RequestMapping(value = "admin/ofertas")
	public String ofertasList(Model model) {
		// Si es una tienda, cargamos las ofertas de esa tienda
		List<Offer> listaOfertas;
		if (Sesion.getInstance().isVendor()) {
			Long vendorid = Sesion.getInstance().getUserLoged().getVendorId();
			listaOfertas = ofertaService.findByTiendaId(vendorid);
		} else {
			listaOfertas = ofertaService.findAll();
		}
		model.addAttribute("vendor", vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId()));
		model.addAttribute("ofertasList", listaOfertas);
		model.addAttribute("oferta", new Offer());
		return "admin/ofertas";
	}

	/**
	 * Mapeo de la página editar ofertas
	 * 
	 * @param model modelo
	 * @param id oferta
	 * @return página
	 **/
	@RequestMapping(value = { "admin/ofertasEdit", "admin/ofertasEdit/{id}" }, method = RequestMethod.GET)
	public String ofertasEditForm(Model model, @PathVariable(required = false, name = "id") Long id) {
		Offer oferta;
		// Añadimos la oferta seleccionada
		if (null != id) {
			oferta=ofertaService.findOne(id);
		} else {
			oferta=new Offer();
		}
		model.addAttribute("ofertaForm", new OfferForm(oferta));

		// Buscamos todos los productos de la tienda para el select 
		model.addAttribute("articlesList", articleService.findByVid(Sesion.getInstance().getUserLoged().getVendorId()));
		
		// Añadimos la tienda
		model.addAttribute("vendor", vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId()));

		return "admin/ofertasEdit";
	}
	
	/**
	 * Mapeo para grabar las ofertas
	 * 
	 * @param model modelo
	 * @param ofertaForm oferta del formulario
	 * @return página
	 **/
	@RequestMapping(value = "admin/ofertasEdit", method = RequestMethod.POST)
	public String ofertasEdit(Model model, @Valid OfferForm ofertaForm) {
		Offer oferta = ofertaForm.getOferta();
		// Recuperamos el artículo
		oferta.setArticulo(articleService.findOne(ofertaForm.getSelectArticleId()));			
		// Establecemos la tienda
		if (oferta.getTiendaId()==null) {
			oferta.setTiendaId(Sesion.getInstance().getUserLoged().getVendorId());
		}
		ofertaService.saveOffer(oferta);
		model.addAttribute("ofertasList", ofertaService.findAll());
		model.addAttribute("vendor", vendorService.findOne(Sesion.getInstance().getUserLoged().getVendorId()));
		return "redirect:/admin/ofertas";
	}	

}
