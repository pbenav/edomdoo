package es.uned.edomdoo.controladores;

import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import es.uned.edomdoo.carrito.CestaLine;
import es.uned.edomdoo.carrito.Carrito;
import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.entidades.Order;
import es.uned.edomdoo.excepcion.NotEnoughProductsInStockException;
import es.uned.edomdoo.servicios.ArticleService;
import es.uned.edomdoo.servicios.BrandService;
import es.uned.edomdoo.servicios.CatService;
import es.uned.edomdoo.servicios.CestaService;
import es.uned.edomdoo.servicios.OrderService;
import es.uned.edomdoo.servicios.ProductService;
import es.uned.edomdoo.servicios.VendorService;

@Controller
@SessionAttributes("cesta")
public class CestaControl {

    private final CestaService cestaService;
    private final ProductService productoService;
    private final OrderService orderService;
    private final ArticleService articleService;
    private final VendorService vendorService;
    private final BrandService brandService;
    private final CatService catService;
    
    /**
     * Constructor
     * @param cestaService servicio cesta
     * @param pS servicio productos
     * @param oS servicio pedidos
     * @param aS servicio artículos
     * @param vS servicio tiendas
     * @param bS servicio marcas
     * @param cS servicio categorías
     */
    @Autowired
    public CestaControl(CestaService cestaService, ProductService pS, OrderService oS, ArticleService aS, VendorService vS, BrandService bS, CatService cS) {
        this.cestaService = cestaService;
        this.productoService = pS;
        this.orderService = oS;
        this.articleService = aS;
        this.vendorService = vS;
        this.brandService = bS;
        this.catService = cS;
    }
	/**
	 * Mapeo de la página cesta
	 * 
	 * @return página
	 **/
    @GetMapping("/cesta")
    public ModelAndView cesta() {
        ModelAndView modelAndView = new ModelAndView("/cesta");
        modelAndView.addObject("cesta", Carrito.getInstance().getCesta());
        modelAndView.addObject("total", Carrito.getInstance().getTotalEuros());
        modelAndView.addObject("marcasList", brandService.findAll());
        modelAndView.addObject("tiendasList", vendorService.findAll());
        modelAndView.addObject("categoriasList", catService.findAll());
        return modelAndView;
    }

	/**
	 * Mapeo para añadir un producto a la cest
	 * 
	 * @param productId producto
	 * @return modelo y vista
	 **/
    @GetMapping("/cesta/addProduct/{productId}")
    public ModelAndView addProductToCart(@PathVariable("productId") Long productId) {
    	Carrito.getInstance().addToCesta(productoService.findById(productId).get());
        return cesta();
    }

	/**
	 * Mapeo para borrar un producto de la cesta
	 * 
	 * @param productId producto 
	 * @return modelo y vista
	 **/
    @GetMapping("/cesta/removeProduct/{productId}")
    public ModelAndView removeProductFromCart(@PathVariable("productId") Long productId) {
    	Carrito.getInstance().delUnitFromCesta(productId);
        return cesta();
    }

	/**
	 * Mapeo de la página compra finalizada
	 * 
	 * @param model modelo
	 * @return página
	 **/
    @GetMapping(value = "cesta/comprafinalizada")
	public String CategoriesList(Model model) {
    	if (Sesion.getInstance().isAnonimousUser()) return "identifiquese";
		if (Carrito.getInstance().getTotal()<=0.0) return "cestavacia";
		// Guardamos el pedido
		for (Map.Entry<Long, CestaLine> entry : Carrito.getInstance().getCesta().entrySet()) {
			Order order = new Order();
			order.setDate(new Date()); // Fecha de la compra
			CestaLine cl = entry.getValue();
			order.setPrecioud(cl.getProducto().getTotal()); // Precio con impuestos y descuentos
			order.setUnidades(new Long(cl.getUnidades())); // Unidades compradas
			order.setVendor(vendorService.findOne(cl.getProducto().getVid()));
			//order.setVid(cl.getProducto().getVid()); // Tienda que venda			
			Article art = articleService.findById(cl.getProducto().getId()).get();
			order.setArticulo(art);
			order.setUser(Sesion.getInstance().getUserLoged()); // Usuario que compra
			orderService.saveOrder(order);	
			// Disminuimos el stock
			art.setStock(art.getStock()-cl.getUnidades());
			articleService.saveArticulo(art);
		}
		Carrito.getInstance().vaciar();
		return "comprafinalizada";
	}
    
    public ModelAndView checkout() {
        try {
        	cestaService.checkout();
        } catch (NotEnoughProductsInStockException e) {
            return cesta().addObject("outOfStockMessage", e.getMessage());
        }
        return cesta();
    }
    
    
}