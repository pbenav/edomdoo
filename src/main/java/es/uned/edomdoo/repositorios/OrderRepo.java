package es.uned.edomdoo.repositorios;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import es.uned.edomdoo.entidades.Order;
import es.uned.edomdoo.entidades.User;
import es.uned.edomdoo.entidades.Vendor;

/**
 * Repositorio de pedidos
 * @author oscar
 *
 */
public interface OrderRepo extends JpaRepository<Order, Long> {

    List<Order> findByUser(User user);
            
    List<Order> findByVendor(Vendor vendor);
    
    List<Order> findByDateAndVendor(Date fecha, Vendor vendor);
    
}
