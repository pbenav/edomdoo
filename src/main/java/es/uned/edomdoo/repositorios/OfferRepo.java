package es.uned.edomdoo.repositorios;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import es.uned.edomdoo.entidades.Offer;

/**
 * Repositorio de ofertas
 * @author Óscar García-Tapia Armada
 *
 */
public interface OfferRepo extends JpaRepository<Offer, Long> {
    
	Optional<Offer> findById(Long id);
    
	List<Offer> findByTiendaId(Long vendorid);
	
}
