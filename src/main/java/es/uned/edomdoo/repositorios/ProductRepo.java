package es.uned.edomdoo.repositorios;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import es.uned.edomdoo.entidades.Product;

/**
 * Repositorio de productos
 * @author oscar
 *
 */
public interface ProductRepo extends JpaRepository<Product, Long> {

	Optional<Product> findById(Long id);

    List<Product> findByBrand(Long brand);
    
    List<Product> findByCategory(Long cat);
    
    List<Product> findByVid(Long vendorid);

    
    @Query("SELECT u FROM Product u ORDER BY cal DESC")
    List<Product> findByCalDesc();
    
}
