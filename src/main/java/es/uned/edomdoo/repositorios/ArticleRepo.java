package es.uned.edomdoo.repositorios;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.entidades.Brand;
import es.uned.edomdoo.entidades.Category;

/**
 * Repositorio de artículos
 * @author pablo
 *
 */
public interface ArticleRepo extends JpaRepository<Article, Long> {

	Optional<Article> findById(Long id);

    List<Article> findByBrand(Brand brand);
    
    List<Article> findByCategory(Category cat);
    
    List<Article> findByVid(Long vendorid);
    
    @Modifying
    @Transactional
    @Query("delete from Article u where id = ?1")
    void deleteUsingSingleQuery(Long id);
    
}
