package es.uned.edomdoo.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import es.uned.edomdoo.entidades.Category;

/**
 * Repositorio de categorías
 * @author pablo
 *
 */
public interface CatRepo extends JpaRepository<Category, Long> {
    Optional<Category> findById(Long id);
}
