package es.uned.edomdoo.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import es.uned.edomdoo.entidades.Brand;

/**
 * Repositorio de marcas
 * @author pablo
 *
 */
public interface BrandRepo extends JpaRepository<Brand, Long> {
    Optional<Brand> findById(Long id);
    /*Optional<Brand> findBySlug(@Param("slug") String slug);*/
}
