package es.uned.edomdoo.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import es.uned.edomdoo.entidades.Vendor;

/**
 * Repositorio de tiendas
 * @author oscargt90
 *
 */
public interface VendorRepo extends JpaRepository<Vendor, Long> {
    Optional<Vendor> findById(@Param("id") Long tienda);
    Optional<Vendor> findByName(@Param("name") String name);
}
