package es.uned.edomdoo.repositorios;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import es.uned.edomdoo.entidades.Score;

/**
 * Repositorio de puntuaciones
 * @author oscar
 *
 */
public interface ScoreRepo extends JpaRepository<Score, Long> {

    List<Score> findByArticleid(Long articleid);
            
}
