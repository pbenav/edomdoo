package es.uned.edomdoo.excepcion;

import es.uned.edomdoo.entidades.Article;

/**
 * Clase que representa una excepción cuando no hay stock
 * @author oscargt90
 *
 */
public class NotEnoughProductsInStockException extends Exception {

	private static final long serialVersionUID = 6089793111140129833L;
	private static final String DEFAULT_MESSAGE = "Not enough products in stock";

	/**
	 * Constructor
	 */
    public NotEnoughProductsInStockException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Constructor
     * @param articulo artículo
     */
    public NotEnoughProductsInStockException(Article articulo) {
        super(String.format("Not enough %s products in stock. Only %d left", articulo.getName(), 0/*, articulo. product.getQuantity()*/));
    }

}
