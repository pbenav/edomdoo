package es.uned.edomdoo.carrito;

import es.uned.edomdoo.entidades.Product;

/**
 * Clase que representa un producto de la cesta
 * 
 * @author oscargt90
 *
 */
public class CestaLine {
	
	private Product producto;
	private int unidades;
	
	/**
	 * Constructor
	 * @param producto producto
	 * @param unidades número de unidades
	 */
	public CestaLine(Product producto, int unidades) {
		super();
		this.producto = producto;
		this.unidades = unidades;
	}
	
	/**
	 * Devuelve el producto
	 * @return producto
	 */
	public Product getProducto() {
		return producto;
	}
	
	/**
	 * Establece el producto
	 * @param producto artículo
	 */
	public void setProducto(Product producto) {
		this.producto = producto;
	}
	
	/**
	 * Devuelve el número de unidades
	 * 
	 * @return unidades
	 */
	public int getUnidades() {
		return unidades;
	}

	/**
	 * Establece el número de unidades
	 * @param unidades uds.
	 */
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	
	/**
	 * Precio total
	 * 
	 * @return total
	 */
	public double getTotal() {
		return producto.getTotal()*unidades;
	}
	
	/**
	 * Devuelve el importe total incluido el símbolo del euro
	 * @return importe total
	 */
	public String getTotalEuros() {
		return String.format("%.2f", producto.getTotal()*unidades) + "€";
	}	
	

}
