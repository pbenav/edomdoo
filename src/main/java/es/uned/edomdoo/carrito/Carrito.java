package es.uned.edomdoo.carrito;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import es.uned.edomdoo.entidades.Product;


/**
 * Clase que nos permite controlar el carrito de la compra de una manera sencilla
 * 
 * Utilizamos el patrón Singleton
 * 
 * @author oscargt90
 *
 */
public class Carrito {
		
	private final Map<Long, CestaLine> cesta = new TreeMap<Long, CestaLine>();
	
	private static final Carrito INSTANCE = new Carrito();
	
	private Carrito() {
	}
	
	/**
	 * Devuelve la única instancia
	 * @return instancia del carrito
	 */
	public static Carrito getInstance() {
		return INSTANCE;
	}
			
	/**
	 * Vacía la cesta
	 */
	public void vaciar() {
		cesta.clear();
	}
	
	/**
	 * Devuelve la cesta
	 * @return cesta
	 */
	public Map<Long, CestaLine> getCesta() {
		return cesta;
	}
	
	/**
	 * Añadimos un producto a la cesta
	 * Si existe, incrementamos una unidad la venta
	 * 
	 * @param p Producto
	 */
	public void addToCesta(Product p) {
		if (p==null) return;
		CestaLine cl = cesta.get(p.getId());
		if (cl==null) {
			cl = new CestaLine(p, 1);		
			// Si hay stock, lo añadimos
			if (hayStock(cl)) cesta.put(p.getId(), cl);
		} else {
			// Incrementamos el número de unidades
			if (hayStock(cl)) cl.setUnidades(cl.getUnidades()+1);
		}
	}
	
	/**
	 * Quitamos un producto de la cesta
	 * 
	 * @param id id del producto a borrar
	 */
	public void delFromCesta(Long id) {
		cesta.remove(id);
	}
	
	/**
	 * Quitamos una unidad de un producto de la cesta
	 * 
	 * @param productid Id del producto
	 */
	public void delUnitFromCesta(Long productid) {
		CestaLine cl = cesta.get(productid);
		if (cl==null) return;
		int ud = cl.getUnidades()-1;
		// Si nos quedamos sin unidades, borramos el produto de la cesta
		if (ud<=0) {
			delFromCesta(productid);
		} else {
			cl.setUnidades(ud);
		}
	}
	
	/**
	 * Incrementa una unidad en la cesta
	 * 
	 * @param productid Id del producto
	 */
	public void addUnitToCesta(Long productid) {
		CestaLine cl = cesta.get(productid);
		if (cl==null) return;
		if (hayStock(cl)) {
			int ud = cl.getUnidades()+1;
			cl.setUnidades(ud);
		}
	}
	
	/**
	 * Calculamos el total
	 * 
	 * @return Importe total calculado
	 */
	public double getTotal() {
		double total = 0.0;
		// Calculamos el total
		for (Entry<Long, CestaLine> entry : cesta.entrySet()) {
			CestaLine cl = entry.getValue();
			total = total + cl.getProducto().getTotal() * cl.getUnidades();
		}
		return total;
	}
	
	/**
	 * Devuelve el total con el símbolo de los euros
	 * 
	 * @return Total euros incluido el símbolo del euro
	 */
	public String getTotalEuros() {
		return String.format("%.2f", getTotal()) + "€";
	}
	
	private boolean hayStock(CestaLine cl) {
		if (cl==null) return false;
		return (cl.getProducto().getStock()>cl.getUnidades());
	}
}
