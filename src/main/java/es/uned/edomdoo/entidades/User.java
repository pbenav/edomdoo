package es.uned.edomdoo.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import es.uned.edomdoo.config.Sesion;

@Entity
@Table(name = "user")
public class User {

    /**
     * Constructor
	 * @param id id
	 * @param email correo electrónico
	 * @param password clave
	 * @param username nombre de usuario
	 * @param name nombre
	 * @param lastname apellidos
	 * @param active activo
	 * @param role rol
	 * @param vid id de tienda, si es que tiene
	 */
	public User(Long id,
			@Email(message = "*Please provide a valid Email") @NotEmpty(message = "*Please provide an email") String email,
			@NotEmpty(message = "*Please provide your password") String password,
			@NotEmpty(message = "*Please provide your name") String username,
			@NotEmpty(message = "*Please provide your name") String name,
			@NotEmpty(message = "*Please provide your last name") String lastname,
			Boolean active, String role, Long vid) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.username = username;
		this.name = name;
		this.lastname = lastname;
		this.active = active;
		this.role = role;
		vendorId = vid;
		
	}

	/**
	 * 
	 */
	public User() {
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "email", unique = true, nullable = false)
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    private String email;

    @Column(name = "password", nullable = false)
    @NotEmpty(message = "*Please provide your password")
    @JsonIgnore
    private String password;

    @Column(name = "username", nullable = false, unique = true)
    @NotEmpty(message = "*Please provide your name")
    private String username;

    @Column(name = "name")
    @NotEmpty(message = "*Please provide your name")
    private String name;

    @Column(name = "lastname")
    @NotEmpty(message = "*Please provide your last name")
    private String lastname;
    
    @Column(name = "active", nullable = false)
    private Boolean active;
    
    @Column(name = "role", nullable = false)
    private String role = Sesion.ROLE_CUSTOMER;
    
    @Column(name = "vendorid")
    private Long vendorId;
    
	/**
	 * Devuelve el id
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id
	 * @param id id del usuario
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Devuelve el correo electrónico
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Establece el correo electrónico
	 * @param email correo electrónico
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Devuelve la clave
	 * @return clave
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Establece la clave
	 * @param password clave
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Devuelve el nombre de usuario
	 * @return nombre de usuario
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Estalece el nombre de usuario
	 * @param username nombre de usuario
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Devuelve el nombre
	 * @return nombre
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece el nombre
	 * @param name nombre
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Devuelve los apellidos
	 * @return apellidos
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Estable los apellidos
	 * @param lastname apellidos
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * Devuelve el nombre completo
	 * @return nombre completo
	 */
	public String getFullName() {
		return name + " " + lastname;
	}
	/**
	 * Devuelve si está activo
	 * @return si o no
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece si está activo o no
	 * @param active activa o desactiva
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * Devuelve el rol 
	 * @return rol
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role Establece el role
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Devuelve el id de la tienda
	 * @return id tienda
	 */
	public Long getVendorId() {
		return vendorId;
	}

	/**
	 * Establece el id de la tienda
	 * @param vendorId id tienda
	 * 
	 */
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

   
}
