/**
 * 
 */
package es.uned.edomdoo.entidades;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * Clase que representa la tabla ofertas
 * @author oscargt90
 * @since 15/05/2019
 * @version 16/05/2019
 */
@Entity
@Table(name = "offer")
public class Offer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	//@Column(name = "offer_type_id", nullable = false)
	//private Long tipoOferta;
	
	@Column(name = "discount", nullable=false)
	private double descuento;

	@Column(name = "shop_id", nullable = false)
	private Long tiendaId;

	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Article articulo;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")	
	@Column(name = "start_date", nullable = false)
	private Date inicio;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name = "end_date", nullable=false)
	private Date fin;

	/**
	 * Constructor
	 */
	public Offer() {
		inicio = new Date();
		fin = new Date();
	}

	/**
	 * Constructor
	 * @param id identificador
	 * @param descuento descuento
	 * @param tiendaid id de la tienda
	 * @param producto artículo
	 * @param inicio fecha de inicio de la oferta
	 * @param fin fecha de fin de la oferta
	 */
	public Offer(Long id, double descuento, Long tiendaid, Article producto, Date inicio, Date fin) {
		super();
		this.id = id;
		this.descuento = descuento;
		this.tiendaId = tiendaid;
		this.articulo = producto;
		this.inicio = inicio;
		this.fin = fin;
	}

	/**
	 * Devuelve el id
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id
	 * @param id identificador
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Devuelve el descuento
	 * @return descuento
	 */
	public double getDescuento() {
		return descuento;
	}

	/**
	 * Establece el descuento
	 * @param descuento descuento
	 */
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	/**
	 * Devuelve la tienda
	 * @return id de la tienda
	 */
	public Long getTiendaId() {
		return tiendaId;
	}

	/**
	 * Establece la tienda
	 * @param tiendaid id de la tienda
	 */
	public void setTiendaId(Long tiendaid) {
		this.tiendaId = tiendaid;
	}

	/**
	 * Devuelve el artículo en oferta
	 * @return artículo
	 */
	public Article getArticulo() {
		return articulo;
	}

	/**
	 * Establece el artículo en oferta
	 * @param producto artículo
	 */
	public void setArticulo(Article producto) {
		this.articulo = producto;
	}

	/**
	 * Devuelve la fecha de inicio de la oferta
	 * @return fecha de inicio
	 */
	public Date getInicio() {
		return inicio;
	}

	/**
	 * Establece la fecha de inicio de la oferta
	 * @param inicio Fecha de inicio
	 */
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	/**
	 * Devuelve la fecha de fin de la oferta
	 * @return fecha de fin
	 */
	public Date getFin() {
		return fin;
	}

	/**
	 * Establece la fecha de fin de la oferta
	 * @param fin fecha de fin
	 */
	public void setFin(Date fin) {
		this.fin = fin;
	}


	
}

	