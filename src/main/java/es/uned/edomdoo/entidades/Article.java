/**
 * 
 */
package es.uned.edomdoo.entidades;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

/**
 * Entidad que representa un artículo
 * @author pablo
 *
 */
@Entity
@Table(name = "article")
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "code", nullable = false)
	@Length(min = 2, message = "** El código debe tener al menos 2 caracteres.")
	private String code;
	
	@ManyToOne( cascade = { CascadeType.ALL } )
	@JoinColumn(name = "brand_id", referencedColumnName = "id", insertable = true, updatable = true)	
	private Brand brand;
	
	@ManyToOne(cascade = { CascadeType.ALL } )
	@JoinColumn(name = "category_id", referencedColumnName = "id", insertable = true, updatable = true)
	private Category category;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(length = 255, name = "description")
	private String description;
	
	@Column(name = "price")
	private BigDecimal price;

	@Column(name = "tax")
	private BigDecimal tax;

	// Calificación en estrellas
	@Column(name = "cal")
	private int cal;

	@Column(name = "img")
	private String img;

	@Column(name = "vendor_id")
	private Long vid;
	
	@Column(name = "stock")
	private Integer stock;

	@Column(name = "features")
	private String features;
	
	/**
	 * 
	 */
	public Article() {
	}


	/**
	 * Constructor de artículos
	 * 
	 * @param id id
	 * @param code código
	 * @param brand marca
	 * @param category categoria
	 * @param name nombre
	 * @param description descrición
	 * @param price precio
	 * @param tax impuestos
	 * @param cal puntuación 
	 * @param img imagen
	 * @param vid id tienda
	 * @param stock stock 
	 * @param features caracteristicas
	 * 
	 */
	public Article(Long id, @Length(min = 2, message = "** El código debe tener al menos 2 caracteres.") String code,
			Brand brand, Category category, String name, String description, BigDecimal price, BigDecimal tax, int cal,
			String img, Long vid, Integer stock, String features) {
		this.id = id;
		this.code = code;
		this.brand = brand;
		this.category = category;
		this.name = name;
		this.description = description;
		this.price = price;
		this.tax = tax;
		this.cal = cal;
		this.img = img;
		this.vid = vid;
		this.stock = stock;
		this.features = features;
	}


	/**
	 * Devuelve el id
	 * @return el id
	 */
	public Long getId() {
		return id;
	}


	/**
	 * Establece el id
	 * @param id identificador
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * Devuelve el código de artículo
	 * @return código
	 */
	public String getCode() {
		return code;
	}


	/**
	 * Establece el código
	 * @param code código
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * Devuelve la marca
	 * @return marca
	 */
	public Brand getBrand() {
		return brand;
	}


	/**
	 * Establece la marca
	 * @param brand marca
	 */
	public void setBrand(Brand brand) {
		this.brand = brand;
	}


	/**
	 * Devuelve la categoría
	 * @return categoría
	 */
	public Category getCategory() {
		return category;
	}


	/**
	 * Establece la categoría
	 * @param category categoría
	 */
	public void setCategory(Category category) {
		this.category = category;
	}


	/**
	 * Devuelve el nombre del artículo
	 * @return nombre del artículo
	 */
	public String getName() {
		return name;
	}


	/**
	 * Establece el nombre del artículo
	 * @param name nombre del artículo
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Devuelve la descripción
	 * @return descripción
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Establece la descripción
	 * @param description descripción
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Devuelve el precio
	 * @return precio
	 */
	public BigDecimal getPrice() {
		return price;
	}


	/**
	 * Establece el precio
	 * @param price precio
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	/**
	 * Devuelve los impuestos
	 * @return impuestos
	 */
	public BigDecimal getTax() {
		return tax;
	}


	/**
	 * Establece los impuestos
	 * @param tax impuestos
	 */
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}


	/**
	 * Devuelve la puntuación
	 * @return puntuación
	 */
	public int getCal() {
		return cal;
	}


	/**
	 * Establece la puntuación
	 * @param cal puntuación
	 */
	public void setCal(int cal) {
		this.cal = cal;
	}


	/**
	 * Devuelve la imagen
	 * @return imagen
	 */
	public String getImg() {
		return img;
	}


	/**
	 * Establece la imagen
	 * @param img imagen
	 */
	public void setImg(String img) {
		this.img = img;
	}


	/**
	 * Devuelve la tienda propietaria del artículo
	 * @return id de la tienda
	 */
	public Long getVid() {
		return vid;
	}


	/**
	 * Establece la tienda propietaria del producto
	 * @param vid id de la tienda
	 */
	public void setVid(Long vid) {
		this.vid = vid;
	}


	/**
	 * Devuelve el stock
	 * @return stock
	 */
	public Integer getStock() {
		return stock;
	}


	/**
	 * Establece el stock
	 * @param stock stock
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	

	/**
	 * Devuelve las características del artículo
	 * @return características
	 */
	public String getFeatures() {
		return features;
	}


	/**
	 * Establece las características del artículo
	 * @param features características
	 */
	public void setFeatures(String features) {
		this.features = features;
	}


	/* (sin Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + cal;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((img == null) ? 0 : img.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((stock == null) ? 0 : stock.hashCode());
		result = prime * result + ((tax == null) ? 0 : tax.hashCode());
		result = prime * result + ((vid == null) ? 0 : vid.hashCode());
		return result;
	}


	/* (sin Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (brand == null) {
			if (other.brand != null)
				return false;
		} else if (!brand.equals(other.brand))
			return false;
		if (cal != other.cal)
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (img == null) {
			if (other.img != null)
				return false;
		} else if (!img.equals(other.img))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (stock == null) {
			if (other.stock != null)
				return false;
		} else if (!stock.equals(other.stock))
			return false;
		if (tax == null) {
			if (other.tax != null)
				return false;
		} else if (!tax.equals(other.tax))
			return false;
		if (vid == null) {
			if (other.vid != null)
				return false;
		} else if (!vid.equals(other.vid))
			return false;
		return true;
	}


	/* (sin Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Article [id=" + id + ", code=" + code + ", brand=" + brand + ", category=" + category + ", name=" + name
				+ ", description=" + description + ", price=" + price + ", tax=" + tax + ", cal=" + cal + ", img=" + img
				+ ", vid=" + vid + ", stock=" + stock + "]";
	}
	
	
	
}