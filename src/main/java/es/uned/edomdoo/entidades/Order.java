/**
 * 
 */
package es.uned.edomdoo.entidades;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Clase que representa un pedido
 * @author oscar
 *
 */
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "date", nullable = false)
	private Date date;
				
	@Column(name = "price")
	private Double precioud;
	
	@Column(name = "units")
	private Long unidades;
	
	@ManyToOne( cascade = { CascadeType.ALL } )
	@JoinColumn(name = "article_id", referencedColumnName = "id")
    private Article articulo;

	@ManyToOne	
    @JoinColumn(name = "vendor_id", referencedColumnName = "id")
    private Vendor vendor;

	@ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;
	
	/**
	 * Constructor
	 */
	public Order() {}
	
	/**
	 * Constructor
	 * @param id identificador
	 * @param date fecha
	 * @param precioud precio por unidad
	 * @param unidades unidades
	 * @param vendor tienda
	 * @param articulo artículo
	 * @param user usuario
	 */
	public Order(Long id, Date date, Double precioud, Long unidades, Vendor vendor, Article articulo, User user) {
		super();
		this.id = id;
		this.date = date;
		this.precioud = precioud;
		this.unidades = unidades;
		this.vendor = vendor;
		this.articulo = articulo;
		this.user = user;
	}
	
	/**
	 * Devuelve el artículo pedido
	 * @return artículo
	 */
	public Article getArticulo() {
		return articulo;
	}

	/**
	 * Establece un artículo en el pedido
	 * @param articulo artículo
	 */
	public void setArticulo(Article articulo) {
		this.articulo = articulo;
	}

	/**
	 * Devuelve el id del pedido
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id del pedido
	 * @param id identificador
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Devuelve la fecha del pedido
	 * @return fecha
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Establece la fecha del pedido
	 * @param date fecha
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Devuelve el precio por unidad
	 * @return precio
	 */
	public Double getPrecioud() {
		return precioud;
	}

	/**
	 * Establece el precio por unidad
	 * @param precioud precio
	 */
	public void setPrecioud(Double precioud) {
		this.precioud = precioud;
	}

	/**
	 * Devuelve el precio con el símbolo del euro
	 * @return precio
	 */
	public String getPrecioudEuros() {
		return String.format("%.2f", getPrecioud()) + "€";
	}
	
	/**
	 * Devuelve el númerode unidades
	 * @return unidades
	 */
	public Long getUnidades() {
		return unidades;
	}

	/**
	 * Establece el número de unidades
	 * @param unidades número de unidades
	 */
	public void setUnidades(Long unidades) {
		this.unidades = unidades;
	}

	/**
	 * Devuelve la tienda
	 * @return tiend
	 */
	public Vendor getVendor() {
		return vendor;
	}

	/**
	 * Establece la tienda
	 * @param vendor tienda
	 */
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	/**
	 * Devuelve el usuario que hace el pedido
	 * @return usuario
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Establece el usuario que hace el pedido
	 * @param user Usuario
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Devuelve el total del pedido
	 * @return total
	 */
	public Double getTotal() {
		return precioud * unidades;
	}
	
	/**
	 * Devuelve el importe total con el símbolo del euro
	 * @return total
	 */
	public String getTotalEuros() {
		return String.format("%.2f", getTotal()) + "€";
	}	
	
}