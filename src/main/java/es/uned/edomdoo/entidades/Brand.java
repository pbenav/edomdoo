package es.uned.edomdoo.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa una marca
 * @author oscargt90
 *
 */
@Entity
@Table(name="brand")
public class Brand {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column
	private String name;
	
	/**
	 * 
	 */
	public Brand() {
	}

	/**
	 * Constructor
	 * @param id identificador
	 * @param name nombre
	 */
	public Brand(Long id, String name) {
		this.id = id;
		this.name = name;

	}

	/**
	 * Devuelve el id
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id
	 * @param id el id a establecer
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Devuelve el nombre
	 * @return nombre
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece el nombre
	 * @param name nombre
	 */
	public void setName(String name) {
		this.name = name;
	}


	/* (sin Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (sin Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Brand other = (Brand) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/* (sin Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Brand [id=" + id + ", name=" + name + "]";
	}
}
