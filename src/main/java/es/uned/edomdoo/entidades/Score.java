/**
 * 
 */
package es.uned.edomdoo.entidades;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Representa la puntuación de un producto
 * @author oscar
 *
 */
@Entity
@Table(name = "scores")
public class Score {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "date", nullable = false)
	private Date date;
				
	@Column(name = "article_id")
	private Long articleid;
	
	@Column(name = "user_id")
	private Long userid;
	
	@Column(name = "score")
	private Integer score;
	

	/**
	 * Constructor
	 */
	public Score() {}


	/**
	 * Constuctor
	 * @param id identificador
	 * @param date fecha
	 * @param articleid artículo
	 * @param userid usuario
	 * @param score puntuación
	 */
	public Score(Long id, Date date, Long articleid, Long userid, Integer score) {
		super();
		this.id = id;
		this.date = date;
		this.articleid = articleid;
		this.userid = userid;
		this.score = score;
	}


	/**
	 * Devuelve el id
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id
	 * @param id identificador
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Devuelve la fecha de la puntuación
	 * @return fecha
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Establece la fecha de la puntuación
	 * @param date fecha
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Devuelve el id del artículo
	 * @return artículo
	 */
	public Long getArticleid() {
		return articleid;
	}

	/**
	 * Establece el id del artículo
	 * @param articleid artículo
	 */
	public void setArticleid(Long articleid) {
		this.articleid = articleid;
	}

	/**
	 * Devuelve el id del usuario
	 * @return id
	 */
	public Long getUserid() {
		return userid;
	}

	/**
	 * Establece el id de usuario
	 * @param userid id
	 */
	public void setUserid(Long userid) {
		this.userid = userid;
	}

	/**
	 * Devuelve la puntuación
 	 * @return puntuación
	 */
	public Integer getScore() {
		return score;
	}

	/**
	 * Establece la puntuación
	 * @param score puntuación
	 */
	public void setScore(Integer score) {
		this.score = score;
	}
	

	
}