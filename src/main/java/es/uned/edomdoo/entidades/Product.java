/**
 * 
 */
package es.uned.edomdoo.entidades;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.data.annotation.Immutable;

/**
 * Clase que represnta la vista de los productos con las ofertas
 * @author oscar
 */
@Entity
@Immutable
@Table(name = "product")
public class Product {

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "cal")
	private int cal;
	
	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "img")
	private String img;
	
	@Column(name = "name")
	private String name;

	@Column(name = "price")
	private BigDecimal price;

	@Column(name = "stock")
	private Integer stock;

	@Column(name = "tax")
	private BigDecimal tax;

	@Column(name = "vendor_id")
	private Long vid;

	@Column(name = "vendorname")
	private String vendorname;
		
	@Column(name = "brand_id")
	private Long brand;
	
	@Column(name = "brandname")
	private String brandname;
		
	@Column(name = "category_id")
	private Long category;

	@Column(name = "categoryname")
	private String categoryname;
	
	@Column(name = "discount")
	private Double discount;
	
	@Column(name = "features")
	private String features;

	@Column(name = "vendorservices")
	private String vendorservices;
	
	/**
	 * Devuelve el id del artículo
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id del artículo
	 * @param id artículo
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Devuelve la puntuación
	 * @return puntuación
	 */
	public int getCal() {
		return cal;
	}

	/**
	 * Establece la puntuación
	 * @param cal puntuación
	 */
	public void setCal(int cal) {
		this.cal = cal;
	}

	/**
	 * Devuelve el código de producto
	 * @return código
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Establece el código de producto
	 * @param code código
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Devuelve la descripción del producto
	 * @return descripción
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece la descripción
	 * @param description descripción
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Devuelve la imagen
	 * @return imagen
	 */
	public String getImg() {
		return img;
	}
		
	/**
	 * Establece la imagen
	 * @param img imagen
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * Devuelve el nombre del producto
	 * @return nombre
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece el nombre del producto
	 * @param name nombre
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Devuelve el precio del producto
	 * @return precio
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * Establece el precio del producto
	 * @param price precio
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * Devuelve el stock del producto
	 * @return stock
	 */
	public Integer getStock() {
		return stock;
	}

	/**
	 * Establece el stock del producto
	 * @param stock stock
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}

	/**
	 * Devuelve los impuestos
	 * @return impuestos
	 */
	public BigDecimal getTax() {
		return tax;
	}

	/**
	 * Establece los impuestos
	 * @param tax impuestos
	 */
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	/**
	 * Devuelve la tienda propietaria del artículo
	 * @return tienda
	 */
	public Long getVid() {
		return vid;
	}

	/**
	 * Establece la tienda propietaria de la tienda
	 * @param vid id de la tienda
	 */
	public void setVid(Long vid) {
		this.vid = vid;
	}

	/**
	 * Devuelve el nombre de la tienda
	 * @return nombre de la tienda
	 */
	public String getVendorname() {
		return vendorname;
	}

	/**
	 * Establece el nombre de la tienda
	 * @param vendorname nombre de la tienda
	 */
	public void setVendorname(String vendorname) {
		this.vendorname = vendorname;
	}

	/**
	 * Devuelve el id de la marca
	 * @return id de la marca
	 */
	public Long getBrand() {
		return brand;
	}

	/**
	 * Establece el id de la marca
	 * @param brand id de la marca
	 */
	public void setBrand(Long brand) {
		this.brand = brand;
	}

	/**
	 * Devuelve el nombre de la marca
	 * @return nombre de la marca
	 */
	public String getBrandname() {
		return brandname;
	}

	/**
	 * Establece el nombre de la marca
	 * @param brandname nombre de la marca
	 */
	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}

	/**
	 * Devuelve la categoría
	 * @return id de la categoría
	 */
	public Long getCategory() {
		return category;
	}

	/**
	 * Establece el id de la categoría
	 * @param category id de la categoría
	 */
	public void setCategory(Long category) {
		this.category = category;
	}

	/**
	 * Devuelve el nombre de la categoría
	 * @return nombre de la categoría
	 */
	public String getCategoryname() {
		return categoryname;
	}

	/**
	 * Establece el nombre de la categoría
	 * @param categoryname nombre de la categoría
	 */
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

	/**
	 * Devuelve el descuento
	 * @return descuento
	 */
	public Double getDiscount() {
		return discount;
	}

	/**
	 * Establece el descuento
	 * @param discount descuento
	 */
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	/**
	 * Devuelve los servicios de la tienda
	 * @return servicios de la tienda
	 */
	public String getVendorservices() {
		return vendorservices;
	}

	/**
	 * Establece los servicios de la tienda
	 * @param vendorservices servicio de tiendas
	 */
	public void setVendorservices(String vendorservices) {
		this.vendorservices = vendorservices;
	}

	/**
	 * Devuelve el precio total, impuestos y descuentos incluidos
	 * @return precio total
	 */
	public Double getTotal() {
		double precio=(price==null?0.0:price.doubleValue()); 
		double iva=(tax==null?0.0:tax.doubleValue());
		double total=0.0;
		if (precio<=0) return 0.0;
		double precioconiva=precio+precio*iva/100;
		double descuento=(discount==null?0.0:discount.doubleValue());
		double descuentototal = (precioconiva*descuento/100);
		total = precioconiva-descuentototal;
		total=(double)Math.round(total * 100d) / 100d;		
		return total;
	}
	
	/**
	 * Devuelve el precio con impuestos incluidos
	 * @return precio con impuestos
	 */
	public Double getPriceWithTax() {
		double precio=(price==null?0.0:price.doubleValue()); 
		double iva=(tax==null?0.0:tax.doubleValue());
		double total=0.0;
		if (precio<=0) return 0.0;
		double precioconiva=precio+precio*iva/100;
		total=(double)Math.round(precioconiva * 100d) / 100d;		
		return total;		
	}
	
	/**
	 * Devuelve el precio con impuestos y el símbolo del euro
	 * @return precio
	 */
	public String getPriceWithTaxEuros() {
		return String.format("%.2f", getPriceWithTax()) + "€";
	}
	
	/**
	 * Devuelve el total con el símbolo del euro
	 * @return total
	 */
	public String getTotalEuros() {
		return String.format("%.2f", getTotal()) + "€";
	}
	
	/**
	 * Si está en oferta
	 * @return true si está en oferta
	 */
	public boolean isInOffer() {
		if (discount==null) return false;
		return discount>0;
	}

	/**
	 * Devuelve las características del artículo
	 * @return características
	 */
	public String getFeatures() {
		return features;
	}

	/**
	 * Establece las características del artículo
	 * @param features características
	 */
	public void setFeatures(String features) {
		this.features = features;
	}

	/**
	 * Constructor
	 */
	public Product() {
	}
	
	/**
	 * Constructor
	 * @param id identificador
	 * @param cal puntuación
	 * @param code código
	 * @param description descripción
	 * @param img imagen
	 * @param name nombre
	 * @param price precio
	 * @param stock stock
	 * @param tax impuestos
	 * @param vid id de la tienda
	 * @param vendorname nombre de la tienda
	 * @param brand id de la marca
	 * @param brandname nombre de la marca
	 * @param category id de la categoría
	 * @param categoryname nombre de la categoría
	 * @param discount descuento
	 * @param features características
	 * @param vendorservices servicios de la tienda
	 */
	public Product(Long id, int cal, String code, String description, String img, String name, BigDecimal price,
			Integer stock, BigDecimal tax, Long vid, String vendorname, Long brand, String brandname, 
			Long category, String categoryname, double discount, String features, String vendorservices) {
		super();
		this.id = id;
		this.cal = cal;
		this.code = code;
		this.description = description;
		this.img = img;
		this.name = name;
		this.price = price;
		this.stock = stock;
		this.tax = tax;
		this.vid = vid;
		this.vendorname = vendorname;
		this.brand = brand;
		this.brandname = brandname;
		this.category = category;
		this.discount = discount;
		this.features = features;
		this.vendorservices = vendorservices;
	}

	/* (sin Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + cal;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((img == null) ? 0 : img.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((stock == null) ? 0 : stock.hashCode());
		result = prime * result + ((tax == null) ? 0 : tax.hashCode());
		result = prime * result + ((vid == null) ? 0 : vid.hashCode());
		return result;
	}

	/* (sin Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (brand == null) {
			if (other.brand != null)
				return false;
		} else if (!brand.equals(other.brand))
			return false;
		if (cal != other.cal)
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (img == null) {
			if (other.img != null)
				return false;
		} else if (!img.equals(other.img))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (stock == null) {
			if (other.stock != null)
				return false;
		} else if (!stock.equals(other.stock))
			return false;
		if (tax == null) {
			if (other.tax != null)
				return false;
		} else if (!tax.equals(other.tax))
			return false;
		if (vid == null) {
			if (other.vid != null)
				return false;
		} else if (!vid.equals(other.vid))
			return false;
		return true;
	}

	/* (sin Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product [id=" + id + ", code=" + code + ", brand=" + brand + ", category=" + category + ", name=" + name
				+ ", description=" + description + ", price=" + price + ", tax=" + tax + ", cal=" + cal + ", img=" + img
				+ ", vid=" + vid + ", stock=" + stock + "]";
	}
	
	
	
}