package es.uned.edomdoo.storage;

import java.io.FileNotFoundException;
import java.net.URL;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.ResourceUtils;

/**
 * Clase que representa las propiedades del repositorio de archivos (imágenes)
 * @author oscargt90
 *
 */
@ConfigurationProperties("storage")
public class StorageProperties {
	
    private String location = "src/main/resources/static/images";
    private URL path;
    
    /**
     * Constructor
     */
	public StorageProperties() {
		try {
			path = ResourceUtils.getURL("classpath:static/images");
		} catch (FileNotFoundException e) {
			System.out.println("\n--Error buscando la carpeta images --\n");
			//e.printStackTrace();
		}
		location = path.toString().split(":", 2)[1];
		System.out.println("\n-- Images location: " + location + " --\n");
	}

	/**
	 * Directorio donde están las imágenes
	 * @return directorio
	 */
	public String getLocation() {
        return location;
    }

	/**
	 * Establece el directorio de las imágenes
	 * @param location directorio
	 */
    public void setLocation(String location) {
        this.location = location;
    }

}
