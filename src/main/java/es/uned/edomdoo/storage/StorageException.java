package es.uned.edomdoo.storage;

/**
 * Clase que representa un error almacenando ficheros
 * @author oscargt90
 *
 */
public class StorageException extends RuntimeException {

	private static final long serialVersionUID = -1050824522334250485L;

	/**
	 * Constructor
	 * @param message mensaje
	 */
	public StorageException(String message) {
        super(message);
    }

	/**
	 * Lanza la excepción
	 * @param message mensaje
	 * @param cause causa
	 */
    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
