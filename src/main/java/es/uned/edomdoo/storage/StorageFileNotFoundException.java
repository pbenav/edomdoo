package es.uned.edomdoo.storage;

/**
 * Clase que representa un error de un archivo no encontrado
 * @author oscargt90
 *
 */
public class StorageFileNotFoundException extends StorageException {

	private static final long serialVersionUID = -8617779269650093697L;

	/**
	 * Constructor
	 * @param message mensaje
	 */
	public StorageFileNotFoundException(String message) {
        super(message);
    }

	/**
	 * Lanza la excepción
	 * @param message mensaje
	 * @param cause causa
	 */
    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}