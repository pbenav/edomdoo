package es.uned.edomdoo.implementacion;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.WebApplicationContext;
import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.excepcion.NotEnoughProductsInStockException;
import es.uned.edomdoo.servicios.CestaService;

/**
 * Servicio para la cesta
 *
 */
@Service
@SessionAttributes("cesta")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class CestaServiceImpl implements CestaService {


    private Map<Article, Integer> articulos = new HashMap<>();


    /**
     * Añade un producto a la cesta
     *
     * @param articulo producto
     */
    @Override
    public void addProduct(Article articulo) {
        if (articulos.containsKey(articulo)) {
        	articulos.replace(articulo, articulos.get(articulo) + 1);
        } else {
        	articulos.put(articulo, 1);
        }
    }

    /**
     * Borrar el artículo de la cesta
     *
     * @param articulo producto
     */
    @Override
    public void removeProduct(Article articulo) {
        if (articulos.containsKey(articulo)) {
            if (articulos.get(articulo) > 1)
            	articulos.replace(articulo, articulos.get(articulo) - 1);
            else if (articulos.get(articulo) == 1) {
            	articulos.remove(articulo);
            }
        }
    }

    /**
     * @return unmodifiable copy of the map
     */
    @Override
    public Map<Article, Integer> getProductsInCart() {
        return Collections.unmodifiableMap(articulos);
    }

    /**
     * rollback si no hay bastantes productos en stock
     *
     * @throws NotEnoughProductsInStockException error
     */
    @Override
    public void checkout() throws NotEnoughProductsInStockException {
/*        Articulo articulo;
        for (Map.Entry<Articulo, Integer> entry : articulos.entrySet()) {
            // Refresh quantity for every product before checking
            articulo = ArticuloRepo.findOne((Long) entry.getKey().getId());
            if (articulo.getCantidad() < entry.getValue())
                throw new NotEnoughProductsInStockException(articulo);
            entry.getKey().setCantidad(articulo.getCantidad() - entry.getValue());
        }
        ArticuloRepo.saveAll(articulos.keySet());
        //ArticuloRepo.s//
        ArticuloRepo.flush();
        articulos.clear();*/
    }
    
    /**
     * Devuelve el total de la cesta
     */
    @Override
    public BigDecimal getTotal() {
        return articulos.entrySet().stream()
                .map(entry -> entry.getKey().getPrice().multiply(BigDecimal.valueOf(entry.getValue())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }
}
