package es.uned.edomdoo.implementacion;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.entidades.Brand;
import es.uned.edomdoo.entidades.Category;
import es.uned.edomdoo.repositorios.ArticleRepo;
import es.uned.edomdoo.servicios.ArticleService;

/**
 * Servicio para acceder al repositorio de artículos
 * @author oscargt90
 *
 */
@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private final ArticleRepo articulosRepo;

	/**
	 * Constructor
	 * @param articulosRepo repositorio de artículos
	 */
	@Autowired
	public ArticleServiceImpl(ArticleRepo articulosRepo) {
		this.articulosRepo = articulosRepo;
	}

	/**
	 * Busca todos los artículos
	 * @return lista de artículos
	 */
	@Override
	public List<Article> findAll() {
		return articulosRepo.findAll();
	}

	/**
	 * Busca todos los artículos de una tienda
	 * @param vendorid id de la tienda
	 * @return lista de artículos
	 */
	@Override
	public List<Article> findByVid(Long vendorid) {
		return articulosRepo.findByVid(vendorid);
	}
	
	/**
	 * Busca un artículo
	 * @param id artículo
	 * @return artículo
	 */
	@Override
	public Article findOne(Long id) {
		return articulosRepo.getOne(id);
	}

	/**
	 * Graba un artículo
	 * @param articulo artículo a grabar
	 * @return artículo grabado
	 */
	@Override
	public Article saveArticulo(Article articulo) {
		return articulosRepo.save(articulo);
	}

	/**
	 * Busca un artículo por id
	 * @param id artículo
	 * @return artículo
	 */
	@Override
	public Optional<Article> findById(Long id) {
		return articulosRepo.findById(id);
	}

	/**
	 * Paginación de los artículos
	 * @param pageable artículos en páginas
	 * @return página de artículos
	 */
	@Override
	public Page<Article> findAllProductsPageable(Pageable pageable) {
		return articulosRepo.findAll(pageable);
	}

	/**
	 * Busca artículos por marca
	 * @param brand marca
	 * @return lista de artículos
	 */
	@Override
	public List<Article> findByBrand(Brand brand) {
		return articulosRepo.findByBrand(brand);
	}
	
	/**
	 * Busca artículos por categoría
	 * @param cat Categoría
	 * @return lista de artículos
	 */
	@Override
	public List<Article> findByCategory(Category cat) {
		return articulosRepo.findByCategory(cat);
	}

	/**
	 * Borrar un artículo
	 * @param id artículo
	 */
	@Override
	public void deleteArticulo(Long id) {
		articulosRepo.deleteUsingSingleQuery(id);

	}	
	
	
	

}
