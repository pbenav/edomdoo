package es.uned.edomdoo.implementacion;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.entidades.Score;
import es.uned.edomdoo.repositorios.ArticleRepo;
import es.uned.edomdoo.repositorios.ScoreRepo;
import es.uned.edomdoo.servicios.ScoreService;

/**
 * Servicio para acceder al repositorio de puntuaciones
 * @author oscargt90
 *
 */
@Service
public class ScoreServiceImpl implements ScoreService {

	@Autowired
	private final ScoreRepo scoreRepo;

	@Autowired
	private final ArticleRepo articleRepo;

	/**
	 * Constructor
	 * @param scoreRepo repositorio de puntuaciones
	 * @param articleRepo repositorio de artículos
	 */
	@Autowired
	public ScoreServiceImpl(ScoreRepo scoreRepo, ArticleRepo articleRepo) {
		this.scoreRepo = scoreRepo;
		this.articleRepo = articleRepo;
	}

	/**
	 * Buscar puntuaciones por id de artículo
	 * @param articleid artículo
	 * @return lista de puntuaciones
	 */
	@Override
	public List<Score> findByArticleid(Long articleid) {
		return scoreRepo.findByArticleid(articleid);
	}

	/**
	 * Añade una nueva puntuación al artículo
	 * @param userid usuario
	 * @param articleid artículo
	 * @param puntuacion puntuación
	 * @return puntuación media
	 */
	@Override
	public int addScore(Long userid, Long articleid, int puntuacion) {
		// Guardamos la puntuación
		Score sc = new Score();
		sc.setDate(new Date());
		sc.setUserid(userid);
		sc.setArticleid(articleid);
		sc.setScore(puntuacion);
		scoreRepo.save(sc);
		// Recalculamos la valoración media
		List<Score> lista = scoreRepo.findByArticleid(articleid);
		int suma= 0;
		// calculamos la suma de todas las puntuaciones
		for (Score score: lista) {
			suma = suma + score.getScore();
		}
		int numvaloraciones=lista.size(); // Número de valoraciones
		// Calculamos la media aritmética
		int media = Math.round(suma/numvaloraciones);
		// Guardamos el resultado
		Article art = articleRepo.getOne(articleid);
		art.setCal(media);
		// Guardamos la media
		articleRepo.save(art);
		return media;
	}
	
	

}
