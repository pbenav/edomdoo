package es.uned.edomdoo.implementacion;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.uned.edomdoo.entidades.Offer;
import es.uned.edomdoo.repositorios.OfferRepo;
import es.uned.edomdoo.servicios.OfferService;

/**
 * Servicio para acceder al repositorio de ofertas
 * @author oscargt90
 *
 */
@Service
public class OfferServiceImpl implements OfferService {

	@Autowired
	private final OfferRepo ofertasRepo;
	
	/**
	 * Constructor
	 * @param ofertasRepo repositorio de ofertas
	 */
	@Autowired
    public OfferServiceImpl(OfferRepo ofertasRepo) {
        this.ofertasRepo = ofertasRepo;
    }

	/**
	 * Buscar todas las ofertas
	 * @return lista de ofertas
	 */
	@Override
	public List<Offer> findAll() {
		return ofertasRepo.findAll();
	}

	/**
	 * Buscar una oferta
	 * @param id oferta
	 * @return oferta
	 */
	@Override
	public Offer findOne(Long id) {
		return ofertasRepo.getOne(id);
	}

	/**
	 * Guardar una oferta
	 * @param oferta oferta
	 * @return oferta
	 */
	@Override
	public Offer saveOffer(Offer oferta) {
		return ofertasRepo.save(oferta);
	}

	/**
	 * Buscar una oferta por id
	 * @param id oferta
	 * @return oferta
	 */
	@Override
	public Optional<Offer> findById(Long id) {
		return ofertasRepo.findById(id);
	}

	/**
	 * Buscar ofertas por tienda
	 * @return lista de ofertas
	 */
	@Override
	public List<Offer> findByTiendaId(Long vid) {
		return ofertasRepo.findByTiendaId(vid);
	}

}
