package es.uned.edomdoo.implementacion;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.uned.edomdoo.entidades.Order;
import es.uned.edomdoo.entidades.User;
import es.uned.edomdoo.entidades.Vendor;
import es.uned.edomdoo.repositorios.OrderRepo;
import es.uned.edomdoo.servicios.OrderService;

/**
 * Servicio para acceder al repositorio de pedidos
 * @author oscargt90
 *
 */
@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private final OrderRepo orderRepo;

	/**
	 * Constructor
	 * @param orderRepo repositorio de pedidos
	 */
	@Autowired
	public OrderServiceImpl(OrderRepo orderRepo) {
		this.orderRepo = orderRepo;
	}

	/**
	 * Buscar un pedido por id
	 * @param id pedido
	 * @return pedido
	 */
	@Override
	public Optional<Order> findById(Long id) {
		return orderRepo.findById(id);
	}

	/**
	 * Buscar pedidos por tienda
	 * @param vendor tienda
	 * @return lista de pedidos
	 */
	@Override
	public List<Order> findByVendor(Vendor vendor) {
		return orderRepo.findByVendor(vendor);
	}
	
	/**
	 * Buscar pedidos por fecha de pedido y tienda
	 * @param fecha de pedido
	 * @param vendor tienda
	 * @return lista de pedidos
	 */
	@Override
	public List<Order> findByDateAndVendor(Date fecha, Vendor vendor){
		return orderRepo.findByDateAndVendor(fecha, vendor);
	}
	
	/**
	 * Buscar los pedidos de un usuario
	 * @param user usuario
	 * @return lista de pedidos
	 */
	@Override
	public List<Order> findByUser(User user) {
		return orderRepo.findByUser(user);
	}

	/**
	 * Grabar un pedido
	 * @param order pedido
	 * @return pedido
	 */
	public Order saveOrder(Order order) {
		return orderRepo.save(order);
	}

	/**
	 * Buscar todos los pedidos
	 * @return lista de pedidos
	 */
	@Override
	public List<Order> findAll() {
		return orderRepo.findAll();
	}

	/**
	 * Borrar un pedido
	 * @param id pedido
	 */
	@Override
	public void deleteOrder(Long id) {
		orderRepo.deleteById(id);	
	}

}
