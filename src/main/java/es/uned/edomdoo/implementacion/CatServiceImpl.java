package es.uned.edomdoo.implementacion;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uned.edomdoo.entidades.Category;
import es.uned.edomdoo.repositorios.CatRepo;
import es.uned.edomdoo.servicios.CatService;

/**
 * Servicio para acceder al repositorio de categorías
 * @author oscargt90
 *
 */
@Service
public class CatServiceImpl implements CatService {

	@Autowired
	private final CatRepo catRepo;

	/**
	 * Constructor
	 * @param cR repositorio de categorías
	 */
	@Autowired
	public CatServiceImpl(CatRepo cR) {
		this.catRepo = cR;
	}

	/**
	 * Busca todas las categorías
	 * @return lista de categorías
	 */
	@Override
	public List<Category> findAll() {
		return catRepo.findAll();
	}

	/**
	 * Busca una categoría
	 * @return categoría
	 */
	@Override
	public Category findOne(Long id) {
		return catRepo.getOne(id);
	}

	/**
	 * Guarda una categoría
	 * @param cat categoría
	 * @return categoría
	 */
	@Override
	public Category saveCategoria(Category cat) {
		return catRepo.save(cat);
	}
	
	/**
	 * Borrar una categoría
	 * @param id categoría
	 */
	@Override
	public void deleteCategoria(Long id) {
		catRepo.deleteById(id);
	}

	/**
	 * Buscar una categoría por id
	 * @param id categoría
	 * @return categoría
	 */
	@Override
	public Optional<Category> findById(Long id) {
		return catRepo.findById(id);
	}
}
