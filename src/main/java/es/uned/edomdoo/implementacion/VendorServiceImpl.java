package es.uned.edomdoo.implementacion;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.uned.edomdoo.entidades.Vendor;
import es.uned.edomdoo.repositorios.VendorRepo;
import es.uned.edomdoo.servicios.VendorService;

/**
 * Servicio para acceder al repositorio de tiendas
 * @author oscargt90
 *
 */
@Service
public class VendorServiceImpl implements VendorService {
	
	@Autowired
	private final VendorRepo vendorRepo;
	
	/**
	 * Constructor
	 * @param tiendaRepo repositorio de tiendas
	 */
	@Autowired
    public VendorServiceImpl(VendorRepo tiendaRepo) {
        this.vendorRepo = tiendaRepo;
    }

	/**
	 * Buscar todas las tiendas
	 * @return lista de tiendas
	 */
	@Override
	public List<Vendor> findAll() {
		return vendorRepo.findAll();
	}

	/**
	 * Buscar una tienda
	 * @param id tienda
	 * @return tienda
	 */
	@Override
	public Vendor findOne(Long id) {
		return vendorRepo.getOne(id);
	}

	/**
	 * Graba los datos de una tienda
	 * @param vendor tienda
	 * @return tienda
	 */
	@Override
	public Vendor saveVendor(Vendor vendor) {
		if (vendor.getActive()==null) {
			vendor.setActive(false);
		}
		return vendorRepo.save(vendor);
	}

	/**
	 * Borra una tienda
	 * @param id tienda
	 */
	@Override
	public void deleteVendor(Long id) {
		vendorRepo.deleteById(id);

	}

	/**
	 * Busca una tienda por id
	 * @param id tienda
	 * @return tienda
	 */
	@Override
	public Optional<Vendor> findById(Long id) {
		return vendorRepo.findById(id);
	}

	/**
	 * Busca todas las tienda con paginación
	 * @param pageable paginación
	 * @return tiendas
	 */
	@Override
	public Page<Vendor> findAllProductsPageable(Pageable pageable) {
		return vendorRepo.findAll(pageable);
	}

	/**
	 * Busca una tienda por nombre
	 * @param vendorname nombre de la tienda
	 * @return tienda
	 */
	@Override
	public Optional<Vendor> findByVendorName(String vendorname) {
		return vendorRepo.findByName(vendorname);
	}

}
