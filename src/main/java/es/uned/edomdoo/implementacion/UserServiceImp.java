package es.uned.edomdoo.implementacion;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import es.uned.edomdoo.config.Sesion;
import es.uned.edomdoo.entidades.User;
import es.uned.edomdoo.repositorios.UserRepository;
import es.uned.edomdoo.servicios.UserService;

/**
 * Servicio para acceder al repositorio de usuarios
 * @author oscargt90
 *
 */
@Service
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    /**
     * Constructor
     * @param userRepository repositorio de usuarios
     * @param passwordEncoder encriptador de claves
     */
    @Autowired
    public UserServiceImp(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        // Inicializamos el singleton Sesion
        // Le pasamos la instancia creada del servicio para que sea más fácil acceder al repositorio de usuarios
        Sesion.getInstance().initialize(this);
    }

    /**
     * Busca un usuario por el nombre de usuario
     * @param username nombre de usuario
     * @return usuario
     */
    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * busca un usuario por el correo electrónico
     * @param email correo electrónico
     * @return usuario
     */
    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    /**
     * Graba un usuario
     * @param user usuario
     * @return usuario
     */
    @Override
    public User saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (user.getActive() == null) {			
        	user.setActive(false);
		}
        if (user.getRole() == null) {			
        	user.setRole("USER");
		}
        return userRepository.saveAndFlush(user);
    }

    /**
     * Busca todos los usuarios
     * @return lista de usuarios
     */
	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	/**
	 * Borrar usuario
	 * @param id usuario
	 */
	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);	
	}

	/**
	 * Buscar un usuario
	 * @param id usuario
	 * @return usuario
	 */
	@Override
	public User findOne(Long id) {
		return userRepository.getOne(id);
	}
}
