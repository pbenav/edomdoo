package es.uned.edomdoo.implementacion;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.uned.edomdoo.entidades.Brand;
import es.uned.edomdoo.repositorios.BrandRepo;
import es.uned.edomdoo.servicios.BrandService;

/**
 * Servicio para acceder al repositorio de marcas
 * @author oscargt90
 *
 */
@Service
public class BrandServiceImpl implements BrandService {

	@Autowired
	private final BrandRepo marcasRepo;

	/**
	 * Constructor
	 * @param marcasRepo repositorio de marcas
	 */
	@Autowired
	public BrandServiceImpl(BrandRepo marcasRepo) {
		this.marcasRepo = marcasRepo;
	}

	/**
	 * Buscar todas las marcas
	 * @return lista de marcas
	 */
	@Override
	public List<Brand> findAll() {
		return marcasRepo.findAll();
	}

	/**
	 * Busca una marca
	 * @param id marca
	 * @return marca
	 */
	@Override
	public Brand findOne(Long id) {
		return marcasRepo.getOne(id);
	}

	/**
	 * Graba una marca
	 * @param marca marca
	 * @return marca
	 */
	@Override
	public Brand saveMarca(Brand marca) {
		return marcasRepo.save(marca);
	}
	
	/**
	 * Borra una marca
	 * @param id marca
	 */
	@Override
	public void deleteMarca(Long id) {
		marcasRepo.deleteById(id);

	}

	/**
	 * Busca una marca por id
	 * @param id marca
	 * @return marca
	 */
	@Override
	public Optional<Brand> findById(Long id) {
		return marcasRepo.findById(id);
	}


}
