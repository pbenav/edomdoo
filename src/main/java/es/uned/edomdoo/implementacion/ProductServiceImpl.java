package es.uned.edomdoo.implementacion;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import es.uned.edomdoo.entidades.Product;
import es.uned.edomdoo.repositorios.ProductRepo;
import es.uned.edomdoo.servicios.ProductService;

/**
 * Servicio para acceder a la vista de productos
 * @author oscargt90
 *
 */
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private final ProductRepo productosRepo;

	/**
	 * Constructor
	 * @param productosRepo repositorio de productos
	 */
	@Autowired
	public ProductServiceImpl(ProductRepo productosRepo) {
		this.productosRepo = productosRepo;
	}
	
	/**
	 * Buscar todos los productos
	 * @return lista de productos
	 */
	@Override
	public List<Product> findAll() {
		return productosRepo.findAll();
	}

	/**
	 * Buscar productos por tienda
	 * @param vendorid tienda
	 * @return lista de productos
	 */
	@Override
	public List<Product> findByVid(Long vendorid) {
		return productosRepo.findByVid(vendorid);
	}
	
	/**
	 * Buscar un producto
	 * @param id producto
	 * @return producto
	 */
	@Override
	public Product findOne(Long id) {
		return productosRepo.getOne(id);
	}

	/**
	 * Buscar producto por id
	 * @param id producto
	 * @return producto
	 */
	@Override
	public Optional<Product> findById(Long id) {
		return productosRepo.findById(id);
	}

	/**
	 * Buscar productos por marca
	 * @param brand marca
	 * @return lista de productos
	 */
	@Override
	public List<Product> findByBrand(Long brand) {
		return productosRepo.findByBrand(brand);
	}
	
	/**
	 * Buscar productos por categoría
	 * @param cat categoría
	 * @return lista de productos
	 */
	@Override
	public List<Product> findByCategory(Long cat) {
		return productosRepo.findByCategory(cat);
	}
	

	@Override
	public List<Product> findByCalDesc() {
		return productosRepo.findByCalDesc();
	}	

}
