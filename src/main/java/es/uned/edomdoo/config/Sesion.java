package es.uned.edomdoo.config;

import java.util.Collection;
import java.util.Optional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import es.uned.edomdoo.carrito.Carrito;
import es.uned.edomdoo.implementacion.UserServiceImp;


/**
 * Clase que nos permite acceder al objeto HTTPSession de una manera sencilla
 * 
 * Utilizamos el patrón Facade y Singleton
 * 
 * @author oscargt90
 *
 */
public class Sesion {
	
	public final static String ROLE_ADMIN="ADMIN";
	public final static String ROLE_VENDOR="VENDOR";
	public final static String ROLE_CUSTOMER="USER";
	
	private UserServiceImp userService;
	
	private static final Sesion INSTANCE = new Sesion();
	
	private Sesion() {
	}
	
	public static Sesion getInstance() {
		return INSTANCE;
	}
	
	public void initialize(UserServiceImp usi) {
		userService=usi;
	}
	
	public String getName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth.getName();
	}
	
	/**
	 * Devuelve true si el usuario logueado es ADMINISTRADOR
	 * @return true si es administrador
	 */
	public boolean isAdmin() {
	     User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	     Collection<? extends GrantedAuthority> authorities  = user.getAuthorities();
	     boolean admin = authorities.contains(new SimpleGrantedAuthority(ROLE_ADMIN));
	     return admin;
	}
		
	/**
	 * Devuelve true si el usuario logueado es TIENDA
	 * @return true si es tienda
	 */
	public boolean isVendor() {
	     User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	     Collection<? extends GrantedAuthority> authorities  = user.getAuthorities();
	     boolean admin = authorities.contains(new SimpleGrantedAuthority(ROLE_VENDOR));
	     return admin;
	}	
	
	/**
	 * Devuelve true si el usuario logueado es CLIENTE
	 * @return true si es cliente
	 */
	public boolean isCustomer() {
	     User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	     Collection<? extends GrantedAuthority> authorities  = user.getAuthorities();
	     boolean admin = authorities.contains(new SimpleGrantedAuthority(ROLE_CUSTOMER));
	     return admin;
	}	
	
	public boolean isAnonimousUser() {
		String username=getName();
		return (username.equals("anonymousUser"));
	}
	
	/**
	 * Devuelve el objeto de tipo es.uned.edomdoo.entidades.User logueado
	 * @return es.uned.edomdoo.entidades.User
	 */
	public es.uned.edomdoo.entidades.User getUserLoged() {
		String username=getName();
		Optional<es.uned.edomdoo.entidades.User> ou = userService.findByUsername(username);
		return ou.get();
	}
	

	public void logout() {
		Carrito.getInstance().vaciar();
	}
	
	
}
