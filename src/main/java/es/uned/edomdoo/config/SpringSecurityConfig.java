package es.uned.edomdoo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	private final AccessDeniedHandler accessDeniedHandler;

	final DataSource dataSource;

	// Tomamos los valores del archivo application.properties
	@Value("${spring.admin.username}")
	private String adminUsername;

	@Value("${spring.admin.password}")
	private String adminPassword;

	@Value("${spring.queries.users-query}")
	private String usersQuery;

	@Value("${spring.queries.roles-query}")
	private String rolesQuery;

	@Autowired
	public SpringSecurityConfig(AccessDeniedHandler accessDeniedHandler, DataSource dataSource) {
		this.accessDeniedHandler = accessDeniedHandler;
		this.dataSource = dataSource;
	}

	/**
	 * Configura la seguridad en los acceso a la web
	 * 
	 * @param http httpsecurity
	 * @throws error
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
				.antMatchers("/resources/**").permitAll()
				.antMatchers("/admin/**").hasAnyAuthority(Sesion.ROLE_ADMIN, Sesion.ROLE_VENDOR)
				.antMatchers("/**").permitAll()
				.anyRequest().authenticated()
				
				.and()
				.formLogin().loginPage("/login").defaultSuccessUrl("/default")
				
				.and()
				.logout().permitAll()
				.and().exceptionHandling().accessDeniedHandler(accessDeniedHandler);
		
		http.headers().frameOptions().disable();		
		http.csrf().disable();
	}
	

	/**
	 * Detalles de autenticación
	 * 
	 * @param auth Gestor de autenticaciones
	 * @throws Exception Error de configuración
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		// Database authentication
		auth.jdbcAuthentication().usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery)
				.dataSource(dataSource).passwordEncoder(passwordEncoder());
		
		// Autenticación base para el usuario admin
		auth.inMemoryAuthentication().withUser(adminUsername).password(adminPassword).roles(Sesion.ROLE_ADMIN);
		
	}

	/**
	 * Configura y devuelve la encriptación de las claves con BCrypt
	 * 
	 * @return clave encritpada
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
