package es.uned.edomdoo.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import es.uned.edomdoo.EdomdooApplication;

public class ServletInitializer extends SpringBootServletInitializer {

	/**
	 * Inicializador de la aplicacón
	 * 
	 * @param application aplicación
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(EdomdooApplication.class);
	}

}
