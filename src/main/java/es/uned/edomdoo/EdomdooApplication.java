package es.uned.edomdoo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import es.uned.edomdoo.storage.StorageProperties;
import es.uned.edomdoo.storage.StorageService;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class EdomdooApplication {
	
	/**
	 * Punto de entrada de la aplicación
	 * @param args argumentos
	 */
	public static void main(String[] args) {
		SpringApplication.run(EdomdooApplication.class, args);	
	}
	
    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            // Ojo - Esto borra el directorio de uploads - No descomentar
        	// storageService.deleteAll();        	
            storageService.init();
        };
    }

}
