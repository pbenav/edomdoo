package es.uned.edomdoo.servicios;

import java.util.List;
import java.util.Optional;

import es.uned.edomdoo.entidades.Category;

public interface CatService {

	Optional<Category> findById(Long id);

	List<Category> findAll();	

	Category findOne(Long id);

	Category saveCategoria(Category id);

	void deleteCategoria(Long id);

}
