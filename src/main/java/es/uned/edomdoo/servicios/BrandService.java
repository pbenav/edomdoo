package es.uned.edomdoo.servicios;

import java.util.List;
import java.util.Optional;
import es.uned.edomdoo.entidades.Brand;

public interface BrandService {

	Optional<Brand> findById(Long id);

	List<Brand> findAll();	

	Brand findOne(Long id);

	Brand saveMarca(Brand id);

	void deleteMarca(Long id);

}
