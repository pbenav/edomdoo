package es.uned.edomdoo.servicios;

import es.uned.edomdoo.entidades.Offer;
import java.util.List;
import java.util.Optional;

/*
 * Interface para las ofertas
 * @author oscargt90
 * @since 15/05/2019
 * @version 16/05/2019
 */
public interface OfferService {

	Optional<Offer> findById(Long id);
	
	List<Offer> findAll();
	
	List<Offer> findByTiendaId(Long vid);

	Offer findOne(Long id);

	Offer saveOffer(Offer oferta);

}
