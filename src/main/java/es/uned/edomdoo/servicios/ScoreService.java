package es.uned.edomdoo.servicios;

import java.util.List;
import es.uned.edomdoo.entidades.Score;

public interface ScoreService {

	List<Score> findByArticleid(Long articleid);
	
	int addScore(Long userid, Long articleid, int puntuacion);
	
}
