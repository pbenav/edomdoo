package es.uned.edomdoo.servicios;

import java.math.BigDecimal;
import java.util.Map;
import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.excepcion.NotEnoughProductsInStockException;

public interface CestaService {

    void addProduct(Article articulo);

    void removeProduct(Article articulo);

    Map<Article, Integer> getProductsInCart();

    void checkout() throws NotEnoughProductsInStockException;
    
    BigDecimal getTotal();
    
}