package es.uned.edomdoo.servicios;

import java.util.List;
import java.util.Optional;
import es.uned.edomdoo.entidades.Product;

public interface ProductService {

	Optional<Product> findById(Long id);

	List<Product> findAll();	

	List<Product> findByBrand(Long brandid);
	
	List<Product> findByVid(Long vendorid);
	
	List<Product> findByCategory(Long cat);
	
	List<Product> findByCalDesc();
	
	Product findOne(Long id);

}
