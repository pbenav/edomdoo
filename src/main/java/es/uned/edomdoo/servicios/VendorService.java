package es.uned.edomdoo.servicios;

import java.util.List;
import java.util.Optional;

import es.uned.edomdoo.entidades.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface VendorService {

	Optional<Vendor> findByVendorName(String vendorname);

	Optional<Vendor> findById(Long id);

	Page<Vendor> findAllProductsPageable(Pageable pageable);

	List<Vendor> findAll();

	Vendor findOne(Long id);

	Vendor saveVendor(Vendor id);

	void deleteVendor(Long id);

}
