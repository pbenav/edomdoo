package es.uned.edomdoo.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import es.uned.edomdoo.entidades.Article;
import es.uned.edomdoo.entidades.Brand;
import es.uned.edomdoo.entidades.Category;

public interface ArticleService {

	Optional<Article> findById(Long id);
	
	Page<Article> findAllProductsPageable(Pageable pageable);

	List<Article> findAll();	

	List<Article> findByBrand(Brand brand);
	
	List<Article> findByVid(Long vendorid);
	
	List<Article> findByCategory(Category cat);
	
	Article findOne(Long id);

	Article saveArticulo(Article id);

	void deleteArticulo(Long id);

}
