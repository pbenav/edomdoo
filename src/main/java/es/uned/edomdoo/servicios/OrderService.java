package es.uned.edomdoo.servicios;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import es.uned.edomdoo.entidades.Order;
import es.uned.edomdoo.entidades.User;
import es.uned.edomdoo.entidades.Vendor;

public interface OrderService {

	Optional<Order> findById(Long id);
	
	List<Order> findByVendor(Vendor vendor);
	
	List<Order> findByDateAndVendor(Date fecha, Vendor vendor);
	
	List<Order> findByUser(User user);
	
	List<Order> findAll();
	
	Order saveOrder(Order order);
	
	void deleteOrder(Long id);

}
