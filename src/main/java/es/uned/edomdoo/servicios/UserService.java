package es.uned.edomdoo.servicios;

import es.uned.edomdoo.entidades.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

	List<User> findAll();

	User findOne(Long id);

	User saveUser(User user);

	void deleteUser(Long id);
}
