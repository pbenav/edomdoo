-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (1, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'pbenav@gmail.com', 'pablo', 'Pablo', 'Sini', true, 'ADMIN', 1);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (2, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'dvinualesp@gmail.com', 'david', 'David', 'David', true, 'ADMIN', 2);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (3, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'oscargt90@gmail.com', 'oscar', 'Oscar', 'Oscar', true, 'ADMIN', 3);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (4, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'aunsincorreo@gmail.com', 'tienda', 'Gestor de Tienda', 'Gestor de Tienda', true, 'VENDOR', 2);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (5, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'juanp@micorreo.com', 'juan', 'Usuario registrado', 'Usuario registrado', true, 'USER', 1);

INSERT INTO BRAND (id, name) VALUES (1, 'Nikon');
INSERT INTO BRAND (id, name) VALUES (2, 'Canon');
INSERT INTO BRAND (id, name) VALUES (3, 'Acer');
INSERT INTO BRAND (id, name) VALUES (4, 'Apple');
INSERT INTO BRAND (id, name) VALUES (5, 'Philips');
INSERT INTO BRAND (id, name) VALUES (6, 'Toshiba');
INSERT INTO BRAND (id, name) VALUES (7, 'Xiaomi');
INSERT INTO BRAND (id, name) VALUES (8, 'Samsung');
INSERT INTO BRAND (id, name) VALUES (9, 'Rowenta');
INSERT INTO BRAND (id, name) VALUES (10, 'Huawey');

INSERT INTO CATEGORIES (id, name) VALUES (1, 'Telefonía');
INSERT INTO CATEGORIES (id, name) VALUES (2, 'Informática');
INSERT INTO CATEGORIES (id, name) VALUES (3, 'Electrodomésticos');
INSERT INTO CATEGORIES (id, name) VALUES (4, 'Televisión');
INSERT INTO CATEGORIES (id, name) VALUES (5, 'Fotografía');

INSERT INTO ARTICLE(id, code, brand, name, description, price, tax, cal, img, vendor_id, stock, category)
VALUES (1, 'NI001', 1, 'Nikon D60', 'Cámara Reflex Nikon D60', 399.99, 21, 3, 'pic2.jpg', 1, 0, 1);
INSERT INTO ARTICLE(id, code, brand, name, description, price, tax, cal, img, vendor_id, stock)
VALUES (2, 'CA001', 2, 'Canon PowerShot', 'Canon PowerShot SX540 HS - Cámara Digital de 20.3 MP', 253.49, 21, 0, 'noimage.jpg', 2, 10);
INSERT INTO ARTICLE(id, code, brand, name, description, price, tax, cal, img, vendor_id, stock)
VALUES (3, 'AC001', 3, 'Acer SLX34', 'Monitor LED Acer SLX34 21\"', 99.99, 21, 4, 'pic3.jpg', 1, 5);
INSERT INTO ARTICLE(id, code, brand, name, description, price, tax, cal, img, vendor_id, stock)
VALUES (4, 'AP0010', 4, 'Apple Iphone 5', 'Apple Iphone 5', 500.49, 21, 5, 'pic4.png', 1, 8);

INSERT INTO VENDOR (id, code, name, active) VALUES (1, 'T001', 'Tienda 0001', true);
INSERT INTO VENDOR (id, code, name, active) VALUES (2, 'T002', 'Tienda 0002', true);
INSERT INTO VENDOR (id, code, name, active) VALUES (3, 'T003', 'Tienda 0003', true);
INSERT INTO VENDOR (id, code, name, active) VALUES (4, 'T004', 'Tienda 0004', true);
INSERT INTO VENDOR (id, code, name, active) VALUES (5, 'T005', 'Tienda 0005', true);
INSERT INTO VENDOR (id, code, name, active) VALUES (6, 'T006', 'Tienda 0006', true);

INSERT INTO OFFER (id, discount, shop_id, product_id, start_date, end_date) VALUES (1, 10.00, 1, 1, '2019-05-01 00:00:00', '2019-06-30 00:00:00');