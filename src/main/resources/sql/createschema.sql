CREATE SEQUENCE SEQUENCE_CATEGORIES START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_ARTICLE START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_VENDOR START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_USER START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_ORDERLIST START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_BRAND START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_ORDERS START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_OFFER START WITH 1 BELONGS_TO_TABLE;
CREATE SEQUENCE SEQUENCE_SCORES START WITH 1 BELONGS_TO_TABLE;

CREATE TABLE ARTICLE(
    ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_ARTICLE) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_ARTICLE,
    CAL INTEGER,
    CODE VARCHAR(255) NOT NULL,
    DESCRIPTION VARCHAR(255),
    IMG VARCHAR(255),
    NAME VARCHAR(255) NOT NULL,
    PRICE DECIMAL(19, 2),
    STOCK INTEGER,
    TAX DECIMAL(19, 2),
    VENDOR_ID BIGINT,
    BRAND_ID BIGINT,
    CATEGORY_ID BIGINT,
    FEATURES VARCHAR(1024)
);

ALTER TABLE ARTICLE ADD CONSTRAINT ARTICLE_PK PRIMARY KEY(ID);

CREATE TABLE BRAND(
    ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_BRAND) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_BRAND,
    NAME VARCHAR(255)
);
ALTER TABLE BRAND ADD CONSTRAINT BRAND_PK PRIMARY KEY(ID);

CREATE TABLE CATEGORIES(
    ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_CATEGORIES) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_CATEGORIES,
    NAME VARCHAR(255)
);
ALTER TABLE CATEGORIES ADD CONSTRAINT CATEGORIES_PK PRIMARY KEY(ID);

CREATE TABLE OFFER(
    ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_OFFER) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_OFFER,
    DISCOUNT DOUBLE NOT NULL,
    END_DATE TIMESTAMP NOT NULL,
    START_DATE TIMESTAMP NOT NULL,
    SHOP_ID BIGINT NOT NULL,
    PRODUCT_ID BIGINT
);
ALTER TABLE OFFER ADD CONSTRAINT OFFER_PK PRIMARY KEY(ID);

CREATE TABLE ORDERS(
    ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_ORDERS) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_ORDERS,
    DATE DATE NOT NULL,
    PRICE DECIMAL(19, 2),
    UNITS BIGINT,
    VENDOR_ID BIGINT,
    ARTICLE_ID BIGINT,    
    USER_ID BIGINT
);
ALTER TABLE ORDERS ADD CONSTRAINT ORDERS_PK PRIMARY KEY(ID);

CREATE TABLE SCORES(
    ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_SCORES) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_SCORES,
    DATE DATE NOT NULL,
    ARTICLE_ID BIGINT,
    USER_ID BIGINT,
    SCORE INTEGER
);
ALTER TABLE SCORES ADD CONSTRAINT SCORES_PK PRIMARY KEY(ID);

CREATE TABLE PUBLIC.USER(
    USER_ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_USER) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_USER,
    ACTIVE BOOLEAN NOT NULL,
    EMAIL VARCHAR(255) NOT NULL,
    LASTNAME VARCHAR(255),
    NAME VARCHAR(255),
    PASSWORD VARCHAR(255) NOT NULL,
    ROLE VARCHAR(255) NOT NULL,
    USERNAME VARCHAR(255) NOT NULL,
    VENDORID BIGINT
);
ALTER TABLE USER ADD CONSTRAINT USER_PK PRIMARY KEY(USER_ID);

CREATE TABLE VENDOR(
    ID BIGINT DEFAULT (NEXT VALUE FOR SEQUENCE_VENDOR) NOT NULL NULL_TO_DEFAULT SEQUENCE SEQUENCE_VENDOR,
    ACTIVE BOOLEAN NOT NULL,
    CODE VARCHAR(255),
    NAME VARCHAR(255),
    SERVICES VARCHAR(1024)
);
ALTER TABLE VENDOR ADD CONSTRAINT VENDOR_PK PRIMARY KEY(ID);

ALTER TABLE USER ADD CONSTRAINT UNQ_USERNAME UNIQUE(USERNAME);
ALTER TABLE VENDOR ADD CONSTRAINT UNQ_CODE UNIQUE(CODE);
ALTER TABLE USER ADD CONSTRAINT UNQ_EMAIL UNIQUE(EMAIL);
ALTER TABLE ARTICLE ADD CONSTRAINT ART_FK_BRAND FOREIGN KEY(BRAND_ID) REFERENCES BRAND(ID) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ARTICLE ADD CONSTRAINT ART_FK_CATEGORY FOREIGN KEY(CATEGORY_ID) REFERENCES CATEGORIES(ID) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ORDERS ADD CONSTRAINT ORDERS_FK_VENDOR FOREIGN KEY(VENDOR_ID) REFERENCES VENDOR(ID) ON UPDATE CASCADE ON DELETE NO ACTION;
ALTER TABLE ORDERS ADD CONSTRAINT ORDERS_FK_USER FOREIGN KEY(USER_ID) REFERENCES PUBLIC.USER(USER_ID) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ORDERS ADD CONSTRAINT ORDERS_FK_ART FOREIGN KEY(ARTICLE_ID) REFERENCES ARTICLE(ID) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE OFFER ADD CONSTRAINT OFFER_FK_ART FOREIGN KEY(PRODUCT_ID) REFERENCES ARTICLE(ID) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SCORES ADD CONSTRAINT SCORE_FK_ART FOREIGN KEY(ARTICLE_ID) REFERENCES ARTICLE(ID) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SCORES ADD CONSTRAINT SCORE_FK_USER FOREIGN KEY(USER_ID) REFERENCES PUBLIC.USER(USER_ID) ON UPDATE CASCADE ON DELETE CASCADE;



-- creamos la vista de los productos para vendre

CREATE VIEW PRODUCT
AS
SELECT ART.*, BRAND.NAME AS BRANDNAME, CATEGORIES.NAME AS CATEGORYNAME, VENDOR.NAME AS VENDORNAME, VENDOR.SERVICES AS VENDORSERVICES, OFFER.DISCOUNT
FROM ARTICLE ART 
LEFT JOIN BRAND ON ART.BRAND_ID=BRAND.ID
LEFT JOIN CATEGORIES ON ART.CATEGORY_ID=CATEGORIES.ID
LEFT JOIN VENDOR ON ART.VENDOR_ID=VENDOR.ID
LEFT JOIN OFFER ON ART.ID=OFFER.PRODUCT_ID
WHERE (OFFER.ID IS NULL) OR (CURRENT_DATE BETWEEN START_DATE AND END_DATE);


-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (1, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'pbenav@gmail.com', 'pablo', 'Pablo', 'Sini', true, 'VENDOR', 1);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (2, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'admin@edomdoo.com', 'admin', 'Admin', 'Admin', true, 'ADMIN', 2);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (3, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'oscargt90@gmail.com', 'oscar', 'Oscar', 'Oscar', true, 'VENDOR', 3);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (4, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'aunsincorreo@gmail.com', 'tienda', 'Gestor de Tienda', 'Gestor de Tienda', true, 'VENDOR', 2);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, name, lastname, active, role, vendorid)
VALUES (5, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'juanp@micorreo.com', 'juan', 'JUAN', 'FERNANDEZ', true, 'USER', 1);

INSERT INTO BRAND (id, name) VALUES (1, 'Nikon');
INSERT INTO BRAND (id, name) VALUES (2, 'Canon');
INSERT INTO BRAND (id, name) VALUES (3, 'Acer');
INSERT INTO BRAND (id, name) VALUES (4, 'Apple');
INSERT INTO BRAND (id, name) VALUES (5, 'Philips');
INSERT INTO BRAND (id, name) VALUES (6, 'Toshiba');
INSERT INTO BRAND (id, name) VALUES (7, 'Sony');
INSERT INTO BRAND (id, name) VALUES (8, 'Samsung');
INSERT INTO BRAND (id, name) VALUES (9, 'Vizio');
INSERT INTO BRAND (id, name) VALUES (10, 'Moulinex');

INSERT INTO CATEGORIES (id, name) VALUES (1, 'Telefonía');
INSERT INTO CATEGORIES (id, name) VALUES (2, 'Informática');
INSERT INTO CATEGORIES (id, name) VALUES (3, 'Electrodomésticos');
INSERT INTO CATEGORIES (id, name) VALUES (4, 'Televisión');
INSERT INTO CATEGORIES (id, name) VALUES (5, 'Fotografía');


INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (1, 'NI001', 1, 'Nikon D60', 'Cámara Reflex Nikon D60', 399.99, 21, 3, 'pic2.jpg', 1, 6, 5, '<ul><li>Pixeles efectivos (megapixeles) 10.2 millones.</li><li>23,6 mm. x 15,8 mm.</li><li>Formato del sensor de imagen. DX.</li><li>SD. SDHC.</li>Velocidad máxima de disparo continuo a máxima resolución. 3 cuadros por segundo.</li><li>ISO 100 - 1600. Hi-1 (ISO 3200)</li><li>Pantalla 2,5 pulg.</li></ul>');
INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (2, 'CA001', 2, 'Canon PowerShot', 'Canon PowerShot SX540 HS - Cámara Digital de 20.3 MP', 253.49, 21, 2, 'noimage.jpg', 2, 10, 5, '<ul><li>Zoom 50x ultra gran angular</li><li>Sensor CMOS de 20,3 megapíxeles</li><li>Pantalla LCD de 7,5 cm (3 pulg.)</li><li>Etiqueta las fotos con el GPS de un smartphone</li><li>Vídeos 1080p más nítidos</li></ul>');
INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (3, 'AC001', 3, 'Acer SLX34', 'Monitor LED Acer SLX34 21\"', 99.99, 21, 4, 'pic3.jpg', 3, 5, 2, '');
INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (4, 'AP0010', 4, 'Apple Iphone 5', 'Apple Iphone 5', 500.49, 21, 4, 'pic4.png', 1, 8, 1, '<ul><li>58,6 mm de ancho</li><li>123,8 mm de alto</li><li>7,6 mm de grosor en su perfil</li><li>peso de 112 g.</li><li>Pantalla de 4 pulgadas</li><li>resolución de 1136 x 640</li><li>pantalla IPS</li></ul>');
INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (5, 'SR0410', 1, 'Cámara de vigilancia Toshiba', 'Cámara de vigilancia Toshiba ST653', 48.99, 21, 3, 'new-pic2.jpg', 1, 8, 4, '<ul><li>16,6 mm de ancho</li><li>123,8 mm de alto</li><li>7,6 mm de grosor en su perfil</li><li>peso de 112 g.</li><li>resolución de 1136 x 640</li></ul>');
INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (6, 'MOU0210', 1, 'Picadora Cuisine XL', 'Picadora/Robot de cocina Moulinex Cuisine XL', 32.99, 21, 3, 'new-pic3.jpg', 1, 2, 4, '<ul><li>186,6 mm de ancho</li><li>323,2 mm de alto</li><li>201,2 mm de profundidad</li><li>peso de 2,2 kg.</li></ul>');
INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (7, 'VI01AC001', 1, 'Monitor VIZIO VIAC001', 'Monitor LED Vizio VIAC001 24\"', 199.99, 21, 1, 'preview-img3.jpg', 1, 20, 2, '<ul><li>Monitor LED con resolución 1600x1200</li></ul>');
INSERT INTO ARTICLE(id, code, brand_id, name, description, price, tax, cal, img, vendor_id, stock, category_id, features)
VALUES (8, 'SONE001', 1, 'Portátil SONY Vaio', 'Ordenador portátil Sony Vaio XLC2231', 899.99, 21, 0, 'preview-img.jpg', 3, 5, 2, '<ul><li>Ordenador portátil SONY Vaio Mod. XLC2231</li><li>Memoria RAM 32GB RAM</li>Discor duro de 4GB HD<li></li></ul>');


INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(1, '2019-05-03', 1, 1, 3);
INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(2, '2019-05-03', 2, 1, 2);
INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(3, '2019-05-03', 3, 1, 4);
INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(4, '2019-05-03', 4, 1, 5);
INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(5, '2019-05-03', 5, 1, 2);
INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(6, '2019-05-03', 6, 1, 1);
INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(7, '2019-05-03', 7, 1, 2);
INSERT INTO SCORES(id, date, article_id, user_id, score) VALUES(8, '2019-05-03', 8, 1, 3);


INSERT INTO VENDOR (id, code, name, active, services) VALUES (1, 'T001', 'ELECTRO MARKET', true, '<ul><li>Envío en 24 horas</li><li>Devoluciones en el plazo de 30 días naturales desde la compra</li><li>Garantía de 24 meses</li><li>Posibilidad de contratar una ampliación de garantía a 5 años</li></ul>');
INSERT INTO VENDOR (id, code, name, active, services) VALUES (2, 'T002', 'INTERNATIONAL SHOP', true, '<ul><li>Envío en 24 horas</li><li>Devoluciones en el plazo de 30 días naturales desde la compra</li><li>Garantía de 24 meses</li><li>Posibilidad de contratar una ampliación de garantía a 5 años</li></ul>');
INSERT INTO VENDOR (id, code, name, active, services) VALUES (3, 'T003', 'SUPER CHOLLO', true, '<ul><li>Envío en 24 horas</li><li>Devoluciones en el plazo de 30 días naturales desde la compra</li><li>Garantía de 24 meses</li><li>Posibilidad de contratar una ampliación de garantía a 5 años</li></ul>');
INSERT INTO VENDOR (id, code, name, active, services) VALUES (4, 'T004', 'EURO MARKET', true, '<ul><li>Envío en 24 horas</li><li>Devoluciones en el plazo de 30 días naturales desde la compra</li><li>Garantía de 24 meses</li><li>Posibilidad de contratar una ampliación de garantía a 5 años</li></ul>');
INSERT INTO VENDOR (id, code, name, active, services) VALUES (5, 'T005', 'TIENDA AZUL', true, '<ul><li>Envío en 24 horas</li><li>Devoluciones en el plazo de 30 días naturales desde la compra</li><li>Garantía de 24 meses</li><li>Posibilidad de contratar una ampliación de garantía a 5 años</li></ul>');
INSERT INTO VENDOR (id, code, name, active, services) VALUES (6, 'T006', 'DON BARATO', true, '<ul><li>Envío en 24 horas</li><li>Devoluciones en el plazo de 30 días naturales desde la compra</li><li>Garantía de 24 meses</li><li>Posibilidad de contratar una ampliación de garantía a 5 años</li></ul>');

INSERT INTO OFFER (id, discount, shop_id, product_id, start_date, end_date) VALUES (1, 10.00, 1, 1, '2019-05-01 00:00:00', '2019-06-30 00:00:00');


INSERT INTO ORDERS (id, date, price, units, vendor_id, article_id, user_id) VALUES (1, '2019-06-02', 120.99, 1, 3, 3, 5);
INSERT INTO ORDERS (id, date, price, units, vendor_id, article_id, user_id) VALUES (2, '2019-06-07', 2012.99, 1, 6, 7, 1);
INSERT INTO ORDERS (id, date, price, units, vendor_id, article_id, user_id) VALUES (3, '2019-06-07', 12.99, 1, 5, 6, 1);

